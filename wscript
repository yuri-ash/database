import os
import subprocess
from Tools.Yscript import BuildHelper

from pathlib import Path

def configure_tools(ctx):
    print('→ configuring database tools in ' + ctx.path.abspath())
    BuildHelper.FindCMake(ctx)
    
    ctx.find_program(filename='swig', var='SWIG', mandatory=True)
    
def prebuild_tools(ctx):
    print('→ building database tools in ' + ctx.path.abspath())
    
    msBuilder = BuildHelper.FindCMakeVSBuilder(ctx)
            
    # our working directory
    execNode = ctx.bldnode.make_node('Tools').make_node('DataBase')
    projectSolution='DataBase.sln'
        
    srcFiles=[]
    srcFiles.extend( [ filename.relative_to(ctx.path.abspath()).as_posix() for filename in Path(os.path.join( ctx.path.abspath(), 'src' ) ).rglob('*.cpp') ] )
    srcFiles.extend( [ filename.relative_to(ctx.path.abspath()).as_posix() for filename in Path(os.path.join( ctx.path.abspath(), 'include') ).rglob('*.h') ] )

    # run SWIG and generate meta code
    wrapperRules = ctx.path.find_node('binder').find_node('DataBase.i') 
    scriptSrc = execNode.make_node('generated').make_node('LuaBinder.cpp')
    swigSrc = [wrapperRules]
    swigSrc.extend(srcFiles)
    ctx(
        rule="${SWIG} -lua -c++ -o " + str(scriptSrc) + " " + str(wrapperRules),
        source=swigSrc,
        target=scriptSrc
    )
        
    #run CMake
    cmakeSrcFiles = [ctx.path.find_node('CMakeLists.txt'), scriptSrc]
    cmakeSrcFiles.extend(srcFiles)
    ctx(cwd=execNode.abspath(), rule='"${CMAKE}" -G "' + msBuilder + '" ' + ctx.path.abspath(), source=cmakeSrcFiles, target=projectSolution)

def build_tools(ctx):
    print('→ building database tools in ' + ctx.path.abspath())
            
    # our working directory
    execNode = ctx.bldnode.make_node('Tools').make_node('DataBase')
    projectSolution='DataBase.sln'
        
    # Get all the files back
    srcFiles=[]
    srcFiles.extend( [ filename.relative_to(ctx.path.abspath()).as_posix() for filename in Path(os.path.join( ctx.path.abspath(), 'src' ) ).rglob('*.cpp') ] )
    srcFiles.extend( [ filename.relative_to(ctx.path.abspath()).as_posix() for filename in Path(os.path.join( ctx.path.abspath(), 'include') ).rglob('*.h') ] )
    srcFiles.append( projectSolution )
    srcFiles.append( execNode.make_node('generated').find_node('LuaBinder.cpp') )
    
    # Build CMake generated solution
    DatabaseExeBuilder = ctx(cwd=execNode.abspath(), name="DataBase", source=srcFiles, target=os.path.join('Debug', 'DataBase.exe'), rule='"${VS_COMPILE} " ' + projectSolution + ' /m')
    
def configure(ctx):
    print('→ configuring Database in ' + ctx.path.abspath())
    
    searchPath=[ ctx.bldnode.find_node('Tools').make_node('DataBase').make_node('Debug').abspath() ]
    ctx.find_program(filename='DataBase', var='DATABASE_BUILDER', mandatory=True, path_list=searchPath )
        