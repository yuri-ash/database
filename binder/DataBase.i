 %module DataBase
 %{
 /* Includes the header in the wrapper code */

#define ID_LEN	64
#define MAX_PATH_LEN 512
#define MAX_TEXT 32
 
#include <list>

#include <iostream>
#include <fstream>
#include "tinyxml/tinyxml.h"

#include "DataBase/Property.h"
#include "DataBase/Class.h"
#include "DataBase/DataCollector.h"
#include "DataBase/ScriptFunctions.h"
#include "DataBase/CustomType.h"
 %}
 
 /* Parse the header file to generate wrappers */
 #define ID_LEN	64
 #define MAX_PATH_LEN 512
 #define MAX_TEXT 32
 %include "../include/DataBase/Property.h"
 %include "../include/DataBase/Class.h"
 %include "../include/DataBase/DataCollector.h"
 %include "../include/DataBase/ScriptFunctions.h"
 %include "../include/DataBase/CustomType.h"
 %include "std_string.i"
 