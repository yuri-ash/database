// XMLBinarizer.cpp : Defines the entry point for the console application.
//

#include "DataBase/DataCollector.h"
#include "DataBase/DataVerification.h"
#include "DataBase/IssueReport.h"
#include "DataBase/XLSWrapper.h"
#include "DataBase/BuiltIn/XMLParser.h"
#include "DataBase/BuiltIn/JSONParser.h"

#include "ArgumentsHandler/ArgumentsHandler.h"
#include <sys/types.h>
#include <sys/stat.h>
#include "dirent.h"

extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
extern int luaopen_DataBase(lua_State* L);
}

DataCollector g_data;

// Built-in parser
XMLParser g_xmlParser;
JSONParser g_jsonParser;

// our list of parser
std::list<Parser*> g_availableParser;


enum FileType
{
	FT_WRAPPER		= 0,
	FT_DEFINITION	= 1,
	FT_DATA			= 2,
	FT_QUERY_ID		= 3
};

ArgumentsHandler arguments;

Parser* FindParser( const std::string& fileName )
{
	auto pos = fileName.rfind( "." ) + 1;
	auto extension = fileName.substr( pos, fileName.size() - pos );

	for ( auto parser : g_availableParser )
	{
		if ( parser->CanReadExtension( extension ) )
		{
			return parser;
		}
	}
	return nullptr;
}

void FindFilesInDir(const char* directory, FileType fileType)
{
    DIR *dir;
	int directoryLen = strlen(directory);
    struct dirent *ent;
    if ((dir = opendir (directory)) != NULL)
    {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL)
        {
            if (ent->d_name[0] != '.')
			{
				char path[MAX_PATH_LEN] = {0};
				strcpy(path, directory);
				if (path[directoryLen - 1] != '\\' && path[directoryLen - 1] != '/')
				{
					strcat(path, "/");
				}
				strcat(path, ent->d_name);

				if (ent->d_type == DT_DIR)
				{
					FindFilesInDir(path, fileType);
				}
				else
				{
					char fileExt[MAX_PATH_LEN] = {0};
					char* lastDot = strrchr(ent->d_name, '.');

					if (lastDot != NULL)
					{
                        int offset = strrchr(ent->d_name, '.') - ent->d_name;
                        strncpy(fileExt, ent->d_name + offset + 1, strlen(ent->d_name) - offset);
					}

					auto parser = FindParser( path );
					if ( parser == nullptr )
					{
						std::cerr << "DataBase > Couldn't find apppropriate parser for file \"" << path << "\"" << endl;
						continue;
					}

					if ( fileType == FT_DEFINITION )
					{
						parser->ParseDefinitionFile( path, &g_data );
					}
					else if ( fileType == FT_DATA )
					{
						parser->ParseDataFile( path, &g_data );
					}
					else if (fileType == FT_WRAPPER)
					{
						XLSWrapper wrapper(parser);
						if ( wrapper.OpenFile(path))
						{
							wrapper.LoadDBFiles(arguments.GetArgumentText("-wrapperDest")->GetValue(0));
							wrapper.Wrap(arguments.GetArgumentText("-xls")->GetValue(0));
						}
					}
					//if (!strcasecmp(fileExt, "xml"))
					//{
					//	std::cout << "DataBase > Parsing file \"" << ent->d_name << "\"\n";

					//	if (fileType == FT_DEFINITION)
					//	{
					//		if ( g_data.OpenFile(path))
					//		{
					//			errs += g_data.ReadClasses();
					//		}
					//		else
					//		{
					//			std::cout << "DataBase ERROR > Parsing of file \"" << ent->d_name << "\" failed!\n";
					//			errs++;
					//		}
					//	}
					//	else if (fileType == FT_DATA)
					//	{
					//		if ( g_data.OpenFile(path))
					//		{
					//			errs += g_data.ReadData();
					//		}
					//		else
					//		{
					//			std::cout << "DataBase ERROR > Parsing of file \"" << ent->d_name << "\" failed!\n";
					//			errs++;
					//		}
					//	}
					//	else if (fileType == FT_WRAPPER)
					//	{
					//		XLSWrapper wrapper;
					//		if ( wrapper.OpenFile(path))
					//		{
					//			wrapper.LoadDBFiles(arguments.GetArgumentText("-wrapperDest")->GetValue(0));
					//			wrapper.Wrap(arguments.GetArgumentText("-xls")->GetValue(0));
					//		}
					//	}
					//}
					//else if (!strcasecmp(fileExt, "java")
					//	|| !strcasecmp(fileExt, "h")
					//	|| !strcasecmp(fileExt, "cs"))
					//{
					//	/*errs +=*/ g_data.QueryExternIDs(path);
					//}
					//else
					//{
					//	std::cout << "DataBase > Ignoring file \"" << ent->d_name << "\"\n";
					//}
				}

			}
        }
        closedir (dir);
    }
    else
    {
        /* could not open directory */
        perror ("");
		LogError("Couldn't open \"%s\".", directory);
    }

}

int main(int argc, char* argv[])
{
	// declare arguments
	arguments.RegisterArgument("-wrapper", ArgumentsHandler::AT_TEXT);
	arguments.RegisterArgument("-wrapperDest", ArgumentsHandler::AT_TEXT);
	arguments.RegisterArgument("-externId", ArgumentsHandler::AT_TEXT);
	arguments.RegisterArgument("-definition", ArgumentsHandler::AT_TEXT);
	arguments.RegisterArgument("-dataBase", ArgumentsHandler::AT_TEXT);
	arguments.RegisterArgument("-dest", ArgumentsHandler::AT_TEXT);
	arguments.RegisterArgument("-xls", ArgumentsHandler::AT_TEXT);
	arguments.RegisterArgument("-export", ArgumentsHandler::AT_TEXT);
	arguments.RegisterArgument("-exportDest", ArgumentsHandler::AT_TEXT);
	arguments.RegisterArgument("-type", ArgumentsHandler::AT_TEXT);
	arguments.RegisterArgument( "-key", ArgumentsHandler::AT_TEXT );
	arguments.RegisterArgument( "-ignore", ArgumentsHandler::AT_TEXT );
	arguments.RegisterArgument( "-validation", ArgumentsHandler::AT_TEXT );
	arguments.ParseStringArguments(argv, argc);

	// register built-in parser
	g_availableParser.push_back( &g_xmlParser );
	g_availableParser.push_back( &g_jsonParser );
	
	TextArgument* arg = NULL;

	arg = arguments.GetArgumentText("-key");
	if (arg->GetCount() > 0)
	{
		g_data.SetEncryptKey(arg->GetValue(0));
	}

	// first, Wrap the XLS into XML format
	std::cout << "DataBase > Wrap XLS into XML...\n";

	arg = arguments.GetArgumentText("-wrapper");
	for(int i = 0; i < arg->GetCount(); ++i)
	{
		FindFilesInDir(arg->GetValue(i), FT_WRAPPER);
	}

	// then, load external ID
	std::cout << "DataBase > Querying external IDs...\n";
	arg = arguments.GetArgumentText("-externId");
	for(int i = 0; i < arg->GetCount(); ++i)
	{
		const char* ext = strrchr(arg->GetValue(i), '.');
		const char* slash = strrchr(arg->GetValue(i), '/');
		const char* backslash = strrchr(arg->GetValue(i), '\\');
		if (ext == NULL
			|| (slash != NULL && ext - slash < 0)
			|| (backslash != NULL && ext - backslash < 0))
		{
			FindFilesInDir(arg->GetValue(i), FT_QUERY_ID);
		}
		else
		{
			g_data.QueryExternIDs(arg->GetValue(i));
		}
	}

	// Setup Ignore property
	std::cout << "DataBase > Setting up ignore list...\n";
	arg = arguments.GetArgumentText( "-ignore" );
	if ( arg->GetCount() > 0 )
	{
		auto parser = FindParser( arg->GetValue( 0 ) );
		parser->SetupIgnoreList( arg->GetValue( 0 ), &g_data );
	}

	//Load type
	std::cout << "DataBase > Load user custom Type...\n";
	arg = arguments.GetArgumentText("-type");
	if (arg->GetCount() <= 0)
	{
		g_data.OpenFile("sample/CustomType.xml");
	}
	else
	{
		g_data.OpenFile(arg->GetValue(0));
	}
	g_data.ReadCustomType();

	// validate
	std::cout << "DataBase > Validate data...\n";
	arg = arguments.GetArgumentText( "-validation" );
	if ( arg->GetCount() > 0 )
	{
		DataVerification::GetDataVerification()->Init( arg->GetValue( 0 ) );
	}

	//Load database
	std::cout << "DataBase > Load DataBase definition...\n";
	arg = arguments.GetArgumentText("-definition");
	for (int i = 0; i < arg->GetCount(); ++i)
	{
		FindFilesInDir(arg->GetValue(i), FT_DEFINITION);
	}
	std::cout << "DataBase > Resolve Parenting...\n";
	g_data.ResolveParenting();

	std::cout << "DataBase > Load DataBase...\n";
	arg = arguments.GetArgumentText("-dataBase");
	for (int i = 0; i < arg->GetCount(); ++i)
	{
		FindFilesInDir(arg->GetValue(i), FT_DATA);
	}
	
	std::cout << "DataBase > Resolve External IDs...\n";
	g_data.ResolveDependanceIDs();

	//export
	std::cout << "DataBase > Export Database...\n";
	arg = arguments.GetArgumentText("-dest");
	for (int i = 0; i < arg->GetCount(); ++i)
	{
		g_data.WriteData(arg->GetValue(i));
	}
	
	// generate helper
	std::cout << "DataBase > Generate helpers...\n";
	TextArgument* arg2 = NULL;
	lua_State *L = luaL_newstate();

    luaL_openlibs(L);
	luaopen_DataBase(L);
	arg = arguments.GetArgumentText("-export");
	arg2 = arguments.GetArgumentText("-exportDest");
	for(int i = 0; i < arg->GetCount(); ++i)
	{
		if (luaL_dofile(L, arg->GetValue(i)) != 0)
		{
			LogError( "%s", lua_tostring( L, -1 ) );
		}
		int currentIndex = lua_gettop( L );
		lua_getglobal(L, "export");

		if (arg2 && i < arg2->GetCount())
		{
#ifdef WIN32
			CreateDirectory(arg2->GetValue(i), NULL);
#else //WIN32
            int status;
            status = mkdir(arg2->GetValue(i), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#endif //WIN32
			lua_pushstring(L, arg2->GetValue(i));
		}
		else
		{
			lua_pushstring(L, "");
		}

		if (lua_pcall(L, 1, 0, 0) != 0)
        {
			LogError( "export %s", lua_tostring( L, -1 ) );
		}

		assert( currentIndex == lua_gettop( L ));

	}

	lua_close(L);

	arg = arguments.GetArgumentText( "-validation" );
	if ( arg->GetCount() > 0 )
	{
		DataVerification::GetDataVerification()->Deinit( );
	}

	IssueReporter::GetReporter().PrintReport();
	return IssueReporter::GetReporter().GetCriticalIssuesCount();
}

