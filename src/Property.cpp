#include "DataBase/Property.h"

std::list<CustomType>	Property::m_customTypes;

Property::Property():
m_type(PT_UNDEF),
m_collection(false)
{
	m_defaultValue = "0";
}

Property::~Property()
{
}

unsigned int Property::GetSize() const
{
	if (m_collection)
	{
		return 0;
	}
	else
	{
		switch(m_type)
		{
		case PT_CUSTOM:
		{
			auto customType = GetCustomType();
			return /*customType->IsArray() ? 0 :*/ customType->GetSize();
		}
		case PT_CLASS:
			return 0;
		default:
			return 0;
		}
	}
}

unsigned int Property::GetCustomSize() const
{
	switch(m_type)
	{
	case PT_CUSTOM:
	{
		auto customType = GetCustomType();
		return customType->GetSize();
	}
	case PT_CLASS:
		return 0;
	default:
		return 0;
	}
}

unsigned int Property::GetBasicSize(int count) const
{
	int size = 0;
	if (m_collection)
	{
		size += 4;
	}
	else
	{
		count = 1;
	}

	switch(m_type)
	{
	case PT_CUSTOM:
	{
		auto customType = GetCustomType();
		size += /*customType->IsArray() ? 0 :*/ customType->GetSize() * count;
	}
		break;
	case PT_CLASS:
		size += 4 * count;
		break;
	default:
		size += 0 * count;
		break;
	}

	return size;
}

void Property::SetType(const char* typeName)
{
	m_type = PT_CLASS;
	m_typeString = typeName;
	const CustomType* type = GetCustomType();

	if (type != NULL)
	{
		m_type = PT_CUSTOM;
		m_typeString = type->GetName();
	}
}

const CustomType* Property::GetCustomType() const
{
	std::list<CustomType>::iterator currentType = m_customTypes.begin();
	while(currentType != m_customTypes.end() && (*currentType).GetName() != m_typeString && (*currentType).GetAlias() != m_typeString )
	{
		++currentType;
	}

	return currentType != m_customTypes.end() ? &(*currentType) : NULL;
}
