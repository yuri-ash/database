#include "DataBase/Data.h"

#include "DataBase/DataCollector.h"
#include "DataBase/DataVerification.h"

#include <sstream>

extern DataCollector g_data;

//Data::Data(Class* relatedClass):
//m_memoryChunk(NULL),
//m_currentSize(0),
//m_relatedClass(relatedClass)
//{
//	m_currentSize = m_relatedClass->GetClassSize();
//	m_memoryChunk = new char[m_currentSize];
//	memset(m_memoryChunk, 0, m_currentSize);
//}

Data::Data(Class* relatedClass):
m_memoryChunk(NULL),
//m_currentSize(0),
m_ID(NULL),
m_relatedClass(relatedClass),
m_parentData(NULL)
{
	//m_currentSize = m_relatedClass->GetClassSize();
	//m_memoryChunk = new char[m_currentSize];
	//InitChunkMemory();
	CreateParentData();
}

Data::~Data()
{
	if (m_memoryChunk)
	{
		delete m_memoryChunk;
	}

	//std::list<StringData*>::iterator currentString = m_subStringData.begin();
	//for(; currentString != m_subStringData.end(); ++currentString)
	//{
	//	delete (*currentString);
	//}

//	m_subStringData.clear();

	m_subData.clear();

	if (m_parentData)
	{
		delete m_parentData;
	}

}

//void Data::InitChunkMemory()
//{
//	memset(m_memoryChunk, 0, m_currentSize);
//
//	const Property* currentProperty = m_relatedClass->FirstProperty();
//	while(currentProperty != NULL)
//	{
//		if ( currentProperty->HasDefaultValue() )
//		{
//			PushData(currentProperty->GetDefaultValue().c_str(), currentProperty);
//		}
//		currentProperty = m_relatedClass->NextProperty();
//	}
//}

void Data::ParseDataID(char* buffer, int& offset, const int ID)
{
	const Property* prop = m_relatedClass->GetProperty("ID");
	if (prop)
	{
		switch(prop->GetCustomSize())
		{
		case 8:
			{
				long long value = (long long)ID;
				memcpy(buffer + offset, &value, prop->GetCustomSize());
			}
			break;
		case 4:
			{
				int value = (int)ID;
				memcpy(buffer + offset, &value, prop->GetCustomSize());
			}
			break;
		case 2:
			{
				short value = (short)ID;
				memcpy(buffer + offset, &value, prop->GetCustomSize());
			}
			break;
		case 1:
			{
				unsigned char value = (char)ID;
				memcpy(buffer + offset, &value, prop->GetCustomSize());
			}
			break;
		}
	}
	offset += 4;
}

void Data::ParseDataString( char* buffer, int& offset, const char* data, const Property* prop )
{
	const CustomType* type = prop->GetCustomType();
	if ( type )
	{
		switch ( type->GetArchetype() )
		{
		case CustomType::Archetype::INTEGER:
			ParseDataInteger( buffer, offset, data, prop );
			break;
		case CustomType::Archetype::FLOAT:
			ParseDataFloat( buffer, offset, data, prop );
			break;
		case CustomType::Archetype::CHARACTER:
			ParseDataCharacter( buffer, offset, data, prop );
			break;
		case CustomType::Archetype::BOOLEAN:
			ParseDataBoolean( buffer, offset, data, prop );
			break;
		default:
			std::cerr << "Unknown archetype !" << std::endl;
			break;
		}
	}
}

void Data::ParseDataChunk( char* buffer, int& offset, const std::list<Data::DataValue::DataChunk>& dataChunk, const Property* prop )
{
	const CustomType* type = prop->GetCustomType();
	if ( type )
	{
		switch ( type->GetArchetype() )
		{
		case CustomType::Archetype::CHARACTER:
			ParseDataCharacter( buffer, offset, dataChunk, prop );
			break;
		default:
			std::cerr << "Unknown archetype !" << std::endl;
			break;
		}
	}
}


int Data::ParseDataInteger( char* buffer, int& offset, const char* data, const Property* prop )
{
#if defined(DATABASE_BUILD_STANDALONE)
    const CustomType* type = prop->GetCustomType();
	if ( type )
	{
		if ( type->IsArray() )
		{
			std::cerr << "Array type not supported for Integer" << std::endl;
			return 1;
		}
		else
		{
			switch ( prop->GetCustomSize() )
			{
			case 8:
			{
				long long value = (long long)g_data.ResolveOperation( data );
				memcpy( buffer + offset, &value, prop->GetCustomSize() );
			}
			break;
			case 4:
			{
				int value = (int)g_data.ResolveOperation( data );
				memcpy( buffer + offset, &value, prop->GetCustomSize() );
			}
			break;
			case 2:
			{
				short value = (short)g_data.ResolveOperation( data );
				memcpy( buffer + offset, &value, prop->GetCustomSize() );
			}
			break;
			case 1:
			{
				unsigned char value = (char)g_data.ResolveOperation( data );
				memcpy( buffer + offset, &value, prop->GetCustomSize() );
			}
			break;
			}
		}
    }
    offset += prop->GetCustomSize();
#endif //DATABASE_BUILD_STANDALONE
	return 0;
}

int Data::ParseDataFloat( char* buffer, int& offset, const char* data, const Property* prop )
{
	const CustomType* type = prop->GetCustomType();
	if ( type )
	{
		if ( type->IsArray() )
		{
			std::cerr << "Array type not supported for Float" << std::endl;
			return 1;
		}
		else
		{
			switch ( prop->GetCustomSize() )
			{
			case 8:
			{
				double value = (double)atof( data );
				memcpy( buffer + offset, &value, prop->GetCustomSize() );
			}
			break;
			case 4:
			{
				float value = (float)atof( data );
				memcpy( buffer + offset, &value, prop->GetCustomSize() );
			}
			break;
			}
		}
	}
	offset += prop->GetCustomSize();
	return 0;
}

int Data::ParseDataCharacter( char* buffer, int& offset, const char* data, const Property* prop )
{
	const CustomType* type = prop->GetCustomType();
	if ( type )
	{
		if ( type->IsArray() )
		{
			unsigned int arraySize = 0;
            arraySize = static_cast<unsigned int>(strlen( data ));
			char* dataVerifiable = new char[arraySize * 2 + 1];
			strcpy( dataVerifiable, data );
			
			DataVerification::GetDataVerification()->IsValid( type->GetName(), dataVerifiable );
            arraySize = static_cast<unsigned int>(strlen( dataVerifiable ));
			memcpy( buffer + offset, &arraySize, sizeof( arraySize ) );
			offset += sizeof( arraySize );
			memcpy( buffer + offset, dataVerifiable, arraySize );
			offset += arraySize;
			//assert( false );
			delete[] dataVerifiable;
		}
		else
		{
			switch ( prop->GetCustomSize() )
			{
			case 2:
			{
                wchar_t value = strlen( data ) > 1 ? static_cast<wchar_t>(data[1]) : static_cast<wchar_t>(data[0]);
				memcpy( buffer + offset, &value, prop->GetCustomSize() );
			}
			break;
			case 1:
			{
				char value = strlen( data ) > 1 ? data[1] : data[0];
				memcpy( buffer + offset, &value, prop->GetCustomSize() );
			}
			break;
			}
			offset += prop->GetCustomSize();
		}
	}
	return 0;
}

int Data::ParseDataCharacter( char* buffer, int& offset, const std::list<Data::DataValue::DataChunk>& dataChunk, const Property* prop )
{
	const CustomType* type = prop->GetCustomType();
	if ( type )
	{
		if ( type->IsArray() )
		{
			switch ( prop->GetCustomSize() )
			{
			case 2:
			{
                unsigned int arraySize = static_cast<unsigned int>(dataChunk.size());
				memcpy( buffer + offset, &arraySize, sizeof( arraySize ) );
				offset += sizeof( arraySize );

				for ( auto data : dataChunk )
				{
					auto charValue = atoi( data.m_dataString.c_str() );
					auto charv = *reinterpret_cast<wchar_t*>(&charValue);

					// laborious copy one by one
					memcpy( buffer + offset, &charv, sizeof( charv ) );
					offset += sizeof( charv );
				}
				assert( false );
			}
			break;
			case 1:
			{
                unsigned int arraySize = static_cast<unsigned int>(dataChunk.size());
				memcpy( buffer + offset, &arraySize, sizeof( arraySize ) );
				offset += sizeof( arraySize );

				for ( auto data : dataChunk )
				{
					auto charValue = atoi( data.m_dataString.c_str() );
					auto charv = *reinterpret_cast<char*>(&charValue);

					// laborious copy one by one
					memcpy( buffer + offset, &charv, sizeof( charv ) );
					offset += sizeof( charv );
				}
				//assert( false );

			}
			break;
			}
			//assert( false );
		}
		else
		{
			assert( false );
		}
	}
	//assert( false );
	return 0;
}

int Data::ParseDataBoolean( char* buffer, int& offset, const char* data, const Property* prop )
{
	const CustomType* type = prop->GetCustomType();
	if ( type )
	{
		if ( type->IsArray() )
		{
			std::cerr << "Array type not supported for Boolean" << std::endl;
			return 1;
		}
		else
		{
			switch ( prop->GetCustomSize() )
			{
			case 4:
			{
				int value = !strcasecmp( data, "true" ) ? 1 : 0;
				memcpy( buffer + offset, &value, prop->GetCustomSize() );
			}
			break;
			case 2:
			{
				short value = !strcasecmp( data, "true" ) ? 1 : 0;
				memcpy( buffer + offset, &value, prop->GetCustomSize() );
			}
			break;
			case 1:
			{
				char value = !strcasecmp( data, "true" ) ? 1 : 0;
				memcpy( buffer + offset, &value, prop->GetCustomSize() );
			}
			break;
			}
		}
	}
	offset += prop->GetCustomSize();
	return 0;
}

int Data::SetID( const char* ID )
{
	//errors accumulator
	int errs = 0;
	ClassID * newID; //const_cast<ClassID*>(m_ID);
	errs += m_relatedClass->AddID( ID, newID );
	m_ID = newID;
	const Property* prop = m_relatedClass->GetProperty( "ID" );
	if ( prop )
	{
		PushSubData( m_ID, prop );
	}
	if ( m_parentData )
	{
		errs += m_parentData->SetID( ID );
	}
	return errs;
}

int Data::ForceDefaultID( )
{
	std::stringstream ss;
	ss << m_relatedClass->GetName() << "_" << m_relatedClass->GetIDs().size();
	return SetID( ss.str().c_str() );
}

void Data::CreateParentData()
{
	if (m_relatedClass->GetParent())
	{
		m_parentData = new Data(m_relatedClass->GetParent());
	}
}

void Data::PushSubData(const ClassID* data, const Property* prop)
{
	std::map<const Property*, DataValue>::iterator subData = m_subData.find(prop);
	if (subData == m_subData.end())
	{
		m_subData.insert(std::make_pair(prop, DataValue(data)));
	}
	else
	{
		subData->second.m_values.push_back( DataValue::DataChunk(data));
	}
}

void Data::PushSubData(const Property* prop, const char* classID)
{
	std::map<const Property*, DataValue>::iterator subData = m_subData.find(prop);
	if (subData == m_subData.end())
	{
		m_subData.insert(std::make_pair(prop, DataValue(classID)));
	}
	else
	{
		subData->second.m_values.push_back( DataValue::DataChunk(classID));
	}

}

Data::DataValue* Data::GetPropertyData( const Property* property )
{
    auto subData = m_subData.find(property);
    return subData != m_subData.end() ? &subData->second : nullptr;
}

const Data::DataValue* Data::GetPropertyData( const Property* property ) const
{
    auto subData = m_subData.find(property);
    return subData != m_subData.end() ? &subData->second : nullptr;
}

bool Data::RemoveSubData( const Property* property, const ClassID& idToRemove)
{
    auto data = GetPropertyData( property );
    if ( data != nullptr )
    {
        for ( auto currentChunk = data->m_values.begin(); currentChunk != data->m_values.end(); ++currentChunk )
        {
            if ( currentChunk->m_dataID->m_stringID == idToRemove.m_stringID )
            {
                data->m_values.erase( currentChunk );
                if (data->m_values.size() == 0)
                {
                    m_subData.erase(m_subData.find(property));
                }
                return true;
            }
        }
    }
    return false;
}

bool Data::RemoveSubData(const Property* property, int32_t index)
{
    auto data = GetPropertyData( property );
    if ( data != nullptr )
    {
        auto valueToRemove = data->m_values.begin();
        auto subDataIndex = 0;
        for ( ; subDataIndex < index && valueToRemove != data->m_values.end(); ++subDataIndex, ++valueToRemove );

        if (subDataIndex == index)
        {
            data->m_values.erase(valueToRemove);
            return true;
        }
    }
    return false;
}

bool Data::RemoveSubData(const Property* property)
{
    auto data = GetPropertyData( property );
    if ( data != nullptr )
    {
        data->m_values.clear();
        return true;
    }
    return false;
}

int Data::GetDataBuffer(char* &buffer)
{
	int bufferSize = CreateDataBuffer();
	int offset = 0;
	FillDataBuffer(m_memoryChunk, offset);
	buffer = m_memoryChunk;

	//assert( bufferSize == offset );
	assert( bufferSize >= offset );

	return offset;
}

void Data::FillDataBuffer(char* buffer, int& offset)
{
	if (GetParentData())
	{
		GetParentData()->FillDataBuffer(buffer, offset);
	}

	std::list<Property>::const_iterator currentProperty = GetClass()->GetProperties().begin();
	for(; currentProperty != GetClass()->GetProperties().end(); ++currentProperty)
	{
		std::map<const Property*, Data::DataValue>::const_iterator currentSubData = GetSubData().find(&(*currentProperty));
		if (currentSubData != GetSubData().end())
		{
            unsigned int count = static_cast<unsigned int>(currentSubData->second.m_values.size());
			if ((*currentProperty).IsCollection())
			{
				memcpy(buffer + offset, (const char*)&count, sizeof(count));
				offset += sizeof(count);

				std::list<Data::DataValue::DataChunk>::const_iterator currentID = currentSubData->second.m_values.begin();
				for(; currentID != currentSubData->second.m_values.end(); ++currentID)
				{
					if ((*currentProperty).GetType() == Property::PT_CLASS)
					{
						if ((*currentID).m_dataID)
						{
							ParseDataID(buffer, offset, (*currentID).m_dataID->m_intID);
						}
						else
						{
							ParseDataID(buffer, offset, 0xFFFFFFFF);
						}
					}
					else if((*currentProperty).GetTypeString() == "id")
					{
						if ((*currentID).m_dataID)
						{
							ParseDataID(buffer, offset, (*currentID).m_dataID->m_intID);
						}
						else
						{
							ParseDataID(buffer, offset, 0xFFFFFFFF);
						}
					}
					else
					{
						ParseDataString(buffer, offset, (*currentID).m_dataString.c_str(), &(*currentProperty));
					}
				}
			}
			else
			{
				if ((*currentProperty).GetType() == Property::PT_CLASS || (*currentProperty).GetName() == "ID")
				{
					if (count > 0)
					{
						assert( (*currentSubData).second.m_values.begin()->m_dataID != nullptr );
						// sanity check
						if ( (*currentSubData).second.m_values.begin()->m_dataID != nullptr )
						{
							ParseDataID( buffer, offset, (*currentSubData).second.m_values.begin()->m_dataID->m_intID );
						}
					}
					else
					{
						ParseDataID(buffer, offset, 0xFFFFFFFF);
					}
				}
				else
				{
					if ( (*currentSubData).second.m_values.size() > 1 )
					{
						assert( currentProperty->GetCustomType() && currentProperty->GetCustomType()->IsString());
						ParseDataChunk( buffer, offset, (*currentSubData).second.m_values, &(*currentProperty) );
					}
					else
					{
						ParseDataString( buffer, offset, (*currentSubData).second.m_values.begin()->m_dataString.c_str(), &(*currentProperty) );
					}
				}
			}
		}
		// set default value
		else
		{
			if ((*currentProperty).IsCollection())
			{
				unsigned int count = 0;
				memcpy(buffer + offset, (const char*)&count, sizeof(count));
				offset += sizeof(count);
			}
			else
			{
				if ((*currentProperty).GetType() == Property::PT_CLASS || (*currentProperty).GetName() == "ID")
				{
					ParseDataID(buffer, offset, 0xFFFFFFFF);
				}
				else
				{
					assert( (*currentProperty).GetCustomType() );
					ParseDataString(buffer, offset, (*currentProperty).GetCustomType()->IsArray() ? "" : (*currentProperty).GetDefaultValue().c_str(), &(*currentProperty));
				}
			}
		}
	}
}

int Data::CreateDataBuffer()
{
	int bufferSize = GetDataSize();
	m_memoryChunk = new char[bufferSize];
	return bufferSize;
}

int Data::GetDataSize()
{
	int size = 0;

	if (GetParentData())
	{
		size += GetParentData()->GetDataSize();
	}

	std::list<Property>::const_iterator currentProperty = GetClass()->GetProperties().begin();
	for(; currentProperty != GetClass()->GetProperties().end(); ++currentProperty)
	{
		std::map<const Property*, DataValue>::iterator currentProp = m_subData.find(&(*currentProperty));
		if (currentProp != m_subData.end())
		{
			int tmpSize = 0;
			
			auto customType = currentProp->first->GetCustomType();
			if ( customType != nullptr && customType->IsArray() )
			{
				if (currentProp->second.m_values.size() != 0)
				{
					std::list<DataValue::DataChunk>::iterator currentData = currentProp->second.m_values.begin();
					for (; currentData != currentProp->second.m_values.end(); ++currentData)
					{
                        tmpSize += static_cast<int>((*currentData).m_dataString.length()) * 2 + 4;
					}
				}
				else
				{
					tmpSize = 4;
				}
			}
			else
			{
                tmpSize = static_cast<int>(currentProp->first->GetBasicSize( static_cast<int>(currentProp->second.m_values.size()) ));
			}

			size += tmpSize;
		}
		else
		{
			int tmpSize = 0;
			auto customType = currentProperty->GetCustomType();
			if ( customType != nullptr && customType->IsArray() )
			{
				// At least 4 for storing array size
				tmpSize = 4;
			}
			else
			{
				tmpSize = currentProperty->GetBasicSize( 0 );
				if ( tmpSize == 0 && !currentProperty->IsCollection() )
				{
					tmpSize = 4;
				}
			}
			size += tmpSize;
		}
	}

	return size;
}
