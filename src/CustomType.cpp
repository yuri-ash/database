#include "DataBase/CustomType.h"
#include "DataBase/DataCollector.h"

#include <cstring>
#include <iostream>

CustomType::CustomType():
	m_size(0),
	m_archetype( Archetype::UNSUPPORTED ),
	m_isArray(false)
{}

CustomType::~CustomType()
{}

int CustomType::SetArchetype(const char* archetype )
{
	//errors accumulator
	int errs = 0;

	if ( !strcasecmp( archetype, "integer" ) )
	{
		m_archetype = Archetype::INTEGER;
	}
	else if ( !strcasecmp( archetype, "float" ) )
	{
		m_archetype = Archetype::FLOAT;
	}
	else if ( !strcasecmp( archetype, "character" ) )
	{
		m_archetype = Archetype::CHARACTER;
	}
	else if ( !strcasecmp( archetype, "boolean" ) )
	{
		m_archetype = Archetype::BOOLEAN;
	}
	else
	{
		std::cerr << "Unknown archetype \"" << archetype << "\"" << std::endl;
		++errs;
	}
	return errs;
}
