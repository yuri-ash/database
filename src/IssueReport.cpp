#include "DataBase/IssueReport.h"

#include <cstdarg>
#include <cwchar>
#include <iostream>
#include <stdio.h>

IssueReporter::IssueReporter()
	: m_criticalIssuesCount( 0 )
	, m_errorsCount( 0 )
	, m_warningsCount( 0 )
	, m_criticalLevel( Severity::S_CRITICAL)
{}

IssueReporter::~IssueReporter()
{}

void IssueReporter::ReportError( Severity status, const wchar_t* errorLog, ... )
{
	if ( static_cast<int32_t>(status) <= static_cast<int32_t>(m_criticalLevel) )
	{
		++m_criticalIssuesCount;
	}
	++m_errorsCount;

	va_list args;
	va_start( args, errorLog );

	vwprintf( errorLog, args );

	va_end( args );
}

void IssueReporter::ReportError( Severity status, const char* errorLog, ... )
{
	if ( static_cast<int32_t>(status) <= static_cast<int32_t>(m_criticalLevel) )
	{
		++m_criticalIssuesCount;
	}
	++m_errorsCount;

	va_list args;
	va_start( args, errorLog );

	vprintf( errorLog, args );

	va_end( args );
}

void IssueReporter::ReportWarning( Severity status, const wchar_t* warningLog, ... )
{
	if ( static_cast<int32_t>(status) <= static_cast<int32_t>(m_criticalLevel) )
	{
		++m_criticalIssuesCount;
	}
	++m_warningsCount;

	va_list args;
	va_start( args, warningLog );

	vwprintf( warningLog, args );

	va_end( args );
}

void IssueReporter::ReportWarning( Severity status, const char* warningLog, ... )
{
	if ( static_cast<int32_t>(status) <= static_cast<int32_t>(m_criticalLevel) )
	{
		++m_criticalIssuesCount;
	}
	++m_warningsCount;

	va_list args;
	va_start( args, warningLog );

	vprintf( warningLog, args );

	va_end( args );
}

void IssueReporter::PrintReport() const
{
	std::cout << "Critical issues:" << m_criticalIssuesCount << std::endl
		<< "Error issues:" << m_errorsCount << std::endl
		<< "Warning issues:" << m_warningsCount << std::endl;
}

static IssueReporter s_reporter;
IssueReporter& IssueReporter::GetReporter()
{
	return s_reporter;
}