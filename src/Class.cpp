#include "DataBase/Class.h"
#include "DataBase/IssueReport.h"

#include <iostream>
#include <cstring>
#include <cstdio>

#define PADDING_STR "padding"

#if defined _WINDOWS
#define strcasecmp(str1, str2)	stricmp(str1, str2)
#endif //_WINDOWS

namespace
{
	// Force MSWindows hash implementation to get same result on different platform
	// FUNCTION _Hash_seq
	inline unsigned int _Hash_seq(const unsigned char *_First, unsigned int _Count)
	{	// FNV-1a hash function for bytes in [_First, _First+_Count)
		const unsigned int _FNV_offset_basis = 2166136261U;
		const unsigned int _FNV_prime = 16777619U;

		unsigned int _Val = _FNV_offset_basis;
		for (unsigned int _Next = 0; _Next < _Count; ++_Next)
		{	// fold in another byte
			_Val ^= (unsigned int)_First[_Next];
			_Val *= _FNV_prime;
		}

		return (_Val);
	}
}

Class::Class():
m_currentSize(0),
m_IDMode(ID_HASH),
m_parent(NULL)
{
	m_currentProperty = m_properties.begin();
	m_currentID = m_IDs.begin();

	// force property ID
	Property id;
	id.SetName("ID");
	id.SetType("id");
	id.SetDefaultValue("-1");
	AddProperty(id);
}

Class::~Class()
{
	m_children.clear();
	m_IDs.clear();
	m_properties.clear();
}

void Class::AddProperty(const Property& prop)
{
	std::list<Property>::iterator currentProperty = m_properties.begin();

	while(currentProperty != m_properties.end() && prop.GetType() >= (*currentProperty).GetType() && prop.GetSize() <= (*currentProperty).GetSize())
	{
		++currentProperty;
	}

	m_properties.insert(currentProperty, prop);

	auto customType = prop.GetCustomType();
	if ( customType == nullptr || !customType->IsArray() )
	{
		m_currentSize += prop.GetSize();
	}
}

//unsigned int Class::GetClassBasicElementSize() const
//{
//	unsigned int size = 0;
//
//	std::list<Property>::const_iterator currentProp = m_properties.begin();
//	for(; currentProp != m_properties.end(); ++currentProp)
//	{
//		size += (*currentProp).GetBasicSize();
//	}
//
//	return size;
//}

const Property* Class::GetProperty(const std::string& name) const
{
	std::list<Property>::const_iterator currentProperty = m_properties.begin();
	for(;currentProperty != m_properties.end(); ++currentProperty)
	{
		if ((*currentProperty).GetName() == name)
		{
			return &(*currentProperty);
		}
	}

	return NULL;
}

const Property* Class::GetPropertyByType(const std::string& name)
{
	std::list<Property>::iterator currentProperty = m_properties.begin();
	for(;currentProperty != m_properties.end(); ++currentProperty)
	{
		if ((*currentProperty).GetTypeString() == name)
		{
			return &(*currentProperty);
		}
	}

	return NULL;
}

//unsigned int Class::GetPropertyOffset(const std::string& name) const
//{
//	unsigned int currentOffset = 0;
//
//	std::list<Property>::const_iterator currentProperty = m_properties.begin();
//	while(currentProperty != m_properties.end() && (*currentProperty).GetName() != name)
//	{
//		currentOffset += (*currentProperty).GetSize();
//		++currentProperty;
//	}
//
//	return currentOffset;
//}

//unsigned int Class::GetPropertySize(const std::string& name)
//{
//	std::list<Property>::iterator currentProperty = m_properties.begin();
//	for(;currentProperty != m_properties.end(); ++currentProperty)
//	{
//		if ((*currentProperty).GetName() == name)
//		{
//			return (*currentProperty).GetSize();
//		}
//	}
//
//	return 0;
//}

int Class::AddID(const std::string& ID, ClassID*& newClassID)
{
	//errors accumulator
	int errs = 0;
	ClassID newID;
	if (!ID.empty())
	{
		std::list<ClassID>::iterator currentID = m_IDs.begin();
		for (; currentID != m_IDs.end(); ++currentID)
		{
			if (!strcasecmp(ID.c_str(), (*currentID).m_stringID.c_str()))
			{
				std::cerr << "DataBase ERROR > Duplicate ID \"" << ID << "\" in class \"" << m_name << "\"!\n";
				errs++;
			}
		}
		newID.m_stringID = ID;
		
        newID.m_intID = _Hash_seq(reinterpret_cast<const unsigned char *>(ID.c_str()), static_cast<unsigned int>(ID.size()));
	}
	else
	{
        newID.m_intID = static_cast<unsigned int>(m_IDs.size());
	}

	m_IDs.push_back(newID);
	newClassID = &(*m_IDs.rbegin());
	return errs;
}

int Class::ValidateIDs()
{
	// test for type cohesion
	//for (int i = 0; i < m_IDs.size(); ++i)
	//{

	//}

	// test for collision
	std::list<ClassID>::iterator currentID = m_IDs.begin();
	for (; currentID != m_IDs.end(); ++currentID)
	{
		std::list<ClassID>::iterator testID = currentID;
		++testID;
		for (; testID != m_IDs.end(); ++testID)
		{
			if (currentID->m_intID == testID->m_intID)
			{
				LogCriticalError( "DataBase ERROR > In class \"%s\", ID collision detected between \"%s\" and \"%s\"!\n", m_name, currentID->m_stringID.c_str(), testID->m_stringID.c_str() );
			}
		}
	}
	return 0;
}

const ClassID* Class::GetID(const std::string& stringID) const
{
	std::list<ClassID>::const_iterator currentID = m_IDs.begin();
	for(; currentID != m_IDs.end(); ++currentID)
	{
		if (!strcasecmp((*currentID).m_stringID.c_str(), stringID.c_str()))
		{
			return &(*currentID);
		}
	}

	return NULL;
}

const ClassID* Class::GetID(int intID) const
{
	std::list<ClassID>::const_iterator currentID = m_IDs.begin();
	for(; currentID != m_IDs.end(); ++currentID)
	{
		if ((*currentID).m_intID == intID)
		{
			return &(*currentID);
		}

	}

	return NULL;
}

void Class::CreatePadding()
{
	unsigned char currentDataSize = 0;
	unsigned char currentPadding = 0;
	currentDataSize = (m_currentSize % 4);

	if (currentDataSize)
	{
		while(currentDataSize < 4)
		{
			Property padding;
			char paddingName[16];
			sprintf(paddingName, "%s%i", PADDING_STR, currentPadding++);
			padding.SetName(paddingName);
			padding.SetType("uint8_t");
			AddProperty(padding);
			++currentDataSize;
		}
	}
}

std::string Class::GetObfuscateName() const
{
	char name[512];
    sprintf(name, "%x", _Hash_seq(reinterpret_cast<const unsigned char *>(m_name.c_str()), static_cast<unsigned int>(m_name.size())));
	return name;
}
