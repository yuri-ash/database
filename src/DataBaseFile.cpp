#include "DataBase/DataBaseFile.h"

#if defined _WINDOWS
#define strcasecmp(str1, str2)	stricmp(str1, str2)
#endif //_WINDOWS

DataBaseFile::DataBaseFile(const char* filePath, const char* fileId)
{
	if(!m_file.LoadFile(filePath))
	{
		const char* dataRoot = "<Data></Data>";
		m_file.Parse( dataRoot );
		m_file.SaveFile(filePath);
	}
	m_id = fileId;
}

DataBaseFile::~DataBaseFile()
{
	m_file.SaveFile();
}

TiXmlElement* DataBaseFile::GetProperty(const char* prop, const char* propID)
{
	TiXmlNode* rootNode = NULL;
	TiXmlElement* rootElement = NULL;

	rootNode = m_file.FirstChild( "Data" );
	assert( rootNode );

	TiXmlElement* xmlElement = NULL;

	xmlElement = GetProperty(rootNode, prop, propID);
	if (!xmlElement)
	{
		TiXmlElement newXmlElement(prop);
		newXmlElement.SetAttribute("id", propID);
		xmlElement = rootNode->InsertEndChild(newXmlElement)->ToElement();
	}


	return xmlElement;
}

TiXmlElement* DataBaseFile::GetProperty(const char* prop, int count)
{
	TiXmlNode* rootNode = NULL;
	TiXmlElement* rootElement = NULL;

	rootNode = m_file.FirstChild( "Data" );
	assert( rootNode );

	TiXmlElement* xmlElement = NULL;

	int currentCount = 0;
	xmlElement = GetProperty(rootNode, prop, count, currentCount);

	if (count != currentCount)
	{
		TiXmlElement newXmlElement(prop);
		if (xmlElement)
		{
			xmlElement = xmlElement->Parent()->InsertAfterChild(xmlElement, newXmlElement)->ToElement();
		}
		else
		{
			xmlElement = rootNode->InsertEndChild(newXmlElement)->ToElement();
		}
	}

	return xmlElement;
}

TiXmlElement* DataBaseFile::GetProperty(TiXmlNode* currentNode, const char* prop, const char* propID)
{

	TiXmlNode* xmlNode = NULL;
	TiXmlElement* xmlElement = NULL;

	xmlElement = currentNode->ToElement();
	if (xmlElement)
	{
		if (!strcasecmp(xmlElement->Value(), prop))
		{
			const char* id = xmlElement->Attribute("id");
			if (id && !strcasecmp(id, propID))
			{
				return xmlElement;
			}
		}
	}

	xmlNode = currentNode->FirstChild();
	xmlElement = NULL;
	while (xmlNode != NULL && xmlElement == NULL)
	{
		xmlElement = GetProperty(xmlNode, prop, propID);
		xmlNode = currentNode->IterateChildren( xmlNode);
	}

	return xmlElement;
}

TiXmlElement* DataBaseFile::GetProperty(TiXmlNode* currentNode, const char* prop, int count, int& currentCount)
{
	TiXmlNode* xmlNode = NULL;
	TiXmlElement* xmlElement = NULL;

	xmlElement = currentNode->ToElement();
	if (xmlElement)
	{
		const char * strDebug = xmlElement->Value();
		if (!strcasecmp(xmlElement->Value(), prop))
		{
			++currentCount;
			return xmlElement;
		}
	}

	xmlNode = currentNode->FirstChild();
	xmlElement = NULL;
	while (xmlNode != NULL && currentCount != count)
	{
		TiXmlElement* xmlReturnElement = GetProperty(xmlNode, prop, count, currentCount);
		if (xmlReturnElement)
		{
			xmlElement = xmlReturnElement;
		}
		xmlNode = currentNode->IterateChildren( xmlNode);
	}

	return xmlElement;
}
