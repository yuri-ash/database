#include "DataBase/IssueReport.h"
#include "DataBase/Utils.h"

#include <codecvt>
#include <cstring>

std::string MakePath(const std::string& part1, const std::string& part2)
{
	std::string path = part1;
	bool part1Ready = *part1.rbegin() == '/' || *part1.rbegin() == '\\';
	bool part2Ready = *part2.begin() == '/' || *part2.begin() == '\\';

	if ( part1Ready && part2Ready )
	{
		path += part2.substr(1);
	}
	else if ( part1Ready || part2Ready )
	{
		path += part2;
	}
	else
	{
		path += "/" + part2;
	}

	return path;
}

bool IsNumber(const char* str, bool allowFloat)
{
	bool isNumber = true;
    int len = static_cast<int>(strlen(str));
	int dotCount = 0;

	int currentChar = 0;
	while(currentChar < len && isNumber)
	{
		if (str[currentChar] < '0' || str[currentChar] > '9' )
		{
			if (str[currentChar] == '-')
			{
				if (currentChar > 0)
				{
					isNumber = false;
				}
			}
			else if (str[currentChar] == '.')
			{
				++dotCount;
				if (!allowFloat || dotCount > 1)
				{
					isNumber = false;
				}
			}
			else
			{
				isNumber = false;
			}
		}
		++currentChar;
	}

	return isNumber;
}

int CutLine(char* line, int wordsPositions[], int maxWords, char separators[])
{
    int strLen = static_cast<int>(strlen(line));
	int wordsCount = 0;
	bool wordStart = false;
	bool commentStart = false;

	for (int i = 0; i < strLen && wordsCount < maxWords; ++i)
	{
		if (!strchr(separators, line[i]))
		{
			if (line[i] == '/' && i + 1 < strLen && line[i + 1] == '/')
			{
				// just comment
				break;
			}
			else if (line[i] == '/' && i + 1 < strLen && line[i + 1] == '*')
			{
				commentStart = true;
			}
			else if (line[i] == '*' && i + 1 < strLen && line[i + 1] == '/')
			{
				commentStart = false;
			}
			else if (!wordStart && !commentStart)
			{
				wordStart = true;
				wordsPositions[wordsCount++] = i;
			}
		}
		else
		{
			if (wordStart)
			{
				wordStart = false;
				line[i] = '\0';
			}
		}
	}

	return wordsCount;
}

char* stoupper( char* s )
{
    char* p = s;
    while (*p = toupper( *p ))
    {
        ++p;
    }
    return s;
}

static void ProcessSpecialCharacter( std::wstring& processString )
{	
	for ( auto pos = 0; pos < processString.length(); ++pos )
	{
		wchar_t currentChar = processString.data()[pos];
		switch ( currentChar )
		{
		case 0x2019: // '
			processString.erase( pos, 1 );
			processString.insert( pos, 1, 0x27 );
			break;
		case 0x2026: // ...
			processString.erase( pos, 1 );
			processString.insert( pos, L"..." );
			break;
		}
	}
}

std::string evaluateCell(YExcel::BasicExcelWorksheet* currentSheet, int line, int column)
{
	std::string result = "";
	YExcel::BasicExcelCell* cell = currentSheet->Cell(line, column);
	switch (cell->Type())
	{
	case YExcel::BasicExcelCell::INT:
		if (true)
		{
			char value[16];
			sprintf(value, "%i", cell->GetInteger());
			result = value;
		}
		break;
	case YExcel::BasicExcelCell::DOUBLE:
		if (true)
		{
			char value[32];
			sprintf(value, "%f", cell->GetDouble());
			result = value;
		}
		break;
		//{
		//	char value[64];
		//	cell->Get(value);
		//	attribute->FirstChild()->ToText()->SetValue(value);
		//}
		//break;
	case YExcel::BasicExcelCell::STRING:
		result = cell->GetString();
		break;
	case YExcel::BasicExcelCell::WSTRING:
	{
		// We got wstring but we expect to output as string.
		// Do it but raise awarness
		std::wstring wStr = cell->GetWString();
		ProcessSpecialCharacter( wStr );
		using convert_type = std::codecvt_utf8<wchar_t>;
		std::wstring_convert<convert_type, wchar_t> converter;
		result = converter.to_bytes( wStr );
		LogError( "MERGER ERROR > Can't evaluate cell at line %d, column %d properly!\n", line + 1, column, cell->GetWString(), result.c_str() );
		LogError( L"MERGER ERROR > Expecting string but got wstring \"%s\", thus converted to \"%S\"\n", cell->GetWString(), result.c_str() );
	}
		break;
	case YExcel::BasicExcelCell::FORMULA:
		if (true)
		{
			const char* str = cell->GetString();
			if (str && strlen(str) > 0)
			{
				result = str;
			}
			else
			{
				char value[16];
				sprintf(value, "%i", cell->GetInteger());
				result = value;
			}
		}
		break;
	case YExcel::BasicExcelCell::UNDEFINED:
		break;
	default:
		{
			LogCriticalError( "MERGER ERROR > Couldn't evaluate cell value at line %d, column %d!\n", line + 1, column );
			result = "0";
		}
		break;
	}

	return result;
}
