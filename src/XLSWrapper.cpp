#include "DataBase/IssueReport.h"
#include "DataBase/XLSWrapper.h"
#include "DataBase/Utils.h"
#include <cstring>
#include <iostream>
#include <sstream>

#define MAX_PATH_LEN 512
#define ID_LEN 64

#if defined _WINDOWS
#define strcasecmp(str1, str2)	stricmp(str1, str2)
#endif //_WINDOWS

Element::Element()
{}
	
Element::~Element()
{}

CellProperty::CellProperty()
{}

CellProperty::~CellProperty()
{
	auto currentProperty = m_properties.begin();
	for (; currentProperty != m_properties.end(); ++currentProperty)
	{
		delete *currentProperty;
	}
	m_properties.clear();
}

bool CellProperty::Evaluate(YExcel::BasicExcelWorksheet* currentSheet, int currentLine, TiXmlElement* wrapElement, unsigned int duplicateID)
{
	bool added = true;

	TiXmlNode* node = wrapElement->FirstChild(m_prop.c_str());
	unsigned int attributeCount = 0;
	while (node && attributeCount < duplicateID - 1)
	{
		node = node->NextSibling(m_prop.c_str());
		++attributeCount;
	}
	if (!node)
	{
		TiXmlElement newAttribute(m_prop.c_str());
		node = wrapElement->InsertEndChild(newAttribute);
	}
	if (!node->FirstChild())
	{
		TiXmlText newText("");
		node->InsertEndChild(newText);
	}

	// add attribute
	TiXmlElement* elem = node->ToElement();
	if (elem)
	{
		if (!m_substituteClass.empty())
		{
			elem->SetAttribute("class", m_substituteClass.c_str());
		}
	}

	std::string result = evaluateCell(currentSheet, currentLine, m_columnID);
	// do not parse child if cell was nullified
	if (strcasecmp(result.c_str(), "NULL") && m_properties.size() > 0)
	{
		std::list<Element*>::iterator currentProp = m_properties.begin();
		bool childAdded = false;
		for (; currentProp != m_properties.end(); ++currentProp)
		{
			childAdded = (*currentProp)->Evaluate(currentSheet, currentLine, node->ToElement(), (*currentProp)->GetDuplicateID()) || childAdded;
		}
		if (!childAdded)
		{
			wrapElement->RemoveChild(node);
		}
	}
	else
	{
		if (result != "")
		{
			node->FirstChild()->ToText()->SetValue(result.c_str());
		}

		// check result value and remove it if not valid
		TiXmlText* textResult = node->FirstChild()->ToText();
		if (textResult && (!strcasecmp(textResult->Value(), "") || !strcasecmp(textResult->Value(), "NULL")))
		{
			wrapElement->RemoveChild(node);
			added = false;
		}
	}

	return added;
}

Switch::Switch()
:m_columnID(0)
{}

Switch::~Switch()
{}

bool Switch::Evaluate(YExcel::BasicExcelWorksheet* currentSheet, int currentLine, TiXmlElement* wrapElement, unsigned int duplicateID)
{
	bool caseEvaluated = false;
	if (m_properties.size() > 0)
	{
		std::string result = evaluateCell(currentSheet, currentLine, m_columnID);
		std::list<Element*>::iterator currentProp = m_properties.begin();
		while (currentProp != m_properties.end() && !caseEvaluated)
		{
			if (Case* currentCase = dynamic_cast<Case*>(*currentProp))
			{
				if (result == currentCase->GetValue())
				{
					caseEvaluated = (*currentProp)->Evaluate(currentSheet, currentLine, wrapElement, duplicateID);
				}
			}
			else
			{
				LogWarning( "DATABASE WARNING > Block other than CASE in SWITCH!\n" );
			}
			++currentProp;
		}
	}
	else
	{
		LogWarning( "DATABASE WARNING > SWITCH block with no CASE!\n" );
	}
	return caseEvaluated;
}

Case::Case()
{}
	
Case::~Case()
{}

bool Case::Evaluate(YExcel::BasicExcelWorksheet* currentSheet, int currentLine, TiXmlElement* wrapElement, unsigned int duplicateID)
{
	bool childAdded = false;
	if (m_properties.size() > 0)
	{
		std::list<Element*>::iterator currentProp = m_properties.begin();
		for (; currentProp != m_properties.end(); ++currentProp)
		{
			childAdded = (*currentProp)->Evaluate(currentSheet, currentLine, wrapElement, duplicateID) || childAdded;
		}
	}

	return childAdded;
}

XLSWrapper::XLSWrapper( Parser* wrapperParser )
	:m_wrapperParser( wrapperParser )
{
}

XLSWrapper::~XLSWrapper()
{
	std::list<DataBaseFile*>::iterator currentFile = m_dbFiles.begin();
	for(; currentFile != m_dbFiles.end(); ++currentFile)
	{
		delete *currentFile;
	}

	m_dbFiles.clear();

}

bool XLSWrapper::OpenFile(const char* fileName)
{
	bool succeed = m_wrapperFile.LoadFile(fileName);

	if (succeed)
	{
		//m_rootNode = m_file.FirstChild( "Data" );
		//assert( m_rootNode );
		//m_rootElement = m_rootNode->ToElement();
		//assert( m_rootElement );
	}
	return succeed;
}

void XLSWrapper::LoadDBFiles(const char* startPathName)
{
	char fullPath[MAX_PATH_LEN] = {0};
    int directoryLen = static_cast<int>(strlen(startPathName));
	strcpy(fullPath, startPathName);
	if (fullPath[directoryLen - 1] != '\\' && fullPath[directoryLen - 1] != '/')
	{
		strcat(fullPath, "/");
	}

	TiXmlNode* rootNode = NULL;
	TiXmlElement* rootElement = NULL;
	TiXmlNode* dbNode = NULL;
	TiXmlElement* dbElement = NULL;

	rootNode = m_wrapperFile.FirstChild( "Wrapper" );
	assert( rootNode );
	rootElement = rootNode->ToElement();
	assert( rootElement );


	dbNode = rootElement->FirstChild("DataBaseFile");
	while (dbNode != NULL)
	{
		dbElement = dbNode->ToElement();

		if (dbElement)
		{
			AddDBFile(dbElement, fullPath);
		}

		dbNode = rootElement->IterateChildren("DataBaseFile", dbNode);

	}
}

void XLSWrapper::AddDBFile(TiXmlElement* fileNode, const char* startPathName)
{
	char id[ID_LEN] = {0};
    int directoryLen = static_cast<int>(strlen(startPathName));
	char fullFilePath[MAX_PATH_LEN] = {0};
	strcpy(fullFilePath, startPathName);

	TiXmlAttribute* fileAttribute = fileNode->FirstAttribute();

	while(fileAttribute)
	{
		if (!strcasecmp(fileAttribute->Name(), "id"))
		{
			strcpy(id, fileAttribute->Value());
		}
		else if (!strcasecmp(fileAttribute->Name(), "name"))
		{
			strcat(fullFilePath, fileAttribute->Value());
		}
		else
		{
			LogError( "XLS WRAPPER > Unknown attribute \"%s\" in element \"%s\".\n", fileAttribute->Name(), fileNode->Value() );
		}

		fileAttribute = fileAttribute->Next();
	}

	m_dbFiles.push_back(new DataBaseFile(fullFilePath, id));
}

void XLSWrapper::Wrap(const char* startPathName)
{
	char fullPath[MAX_PATH_LEN] = {0};
    int directoryLen = static_cast<int>(strlen(startPathName));
	strcpy(fullPath, startPathName);
	if (fullPath[directoryLen - 1] != '\\' && fullPath[directoryLen - 1] != '/')
	{
		strcat(fullPath, "/");
	}

	TiXmlNode* rootNode = NULL;
	TiXmlElement* rootElement = NULL;
	TiXmlNode* xlsNode = NULL;
	TiXmlElement* xlsElement = NULL;

	rootNode = m_wrapperFile.FirstChild( "Wrapper" );
	assert( rootNode );
	rootElement = rootNode->ToElement();
	assert( rootElement );

	xlsNode = rootElement->FirstChild("ExcelFile");
	while (xlsNode != NULL)
	{
		xlsElement = xlsNode->ToElement();

		if (xlsElement)
		{
			WrapExcelFile(xlsElement, fullPath);
		}

		xlsNode = rootElement->IterateChildren("ExcelFile", xlsNode);

	}
}

void XLSWrapper::WrapExcelFile(TiXmlElement* xlsElement, const char* startPathName)
{
	char id[ID_LEN] = {0};
    int directoryLen = static_cast<int>(strlen(startPathName));
	char fullFilePath[MAX_PATH_LEN] = {0};
	strcpy(fullFilePath, startPathName);

	TiXmlAttribute* fileAttribute = xlsElement->FirstAttribute();

	while(fileAttribute)
	{
		if (!strcasecmp(fileAttribute->Name(), "name"))
		{
			strcat(fullFilePath, fileAttribute->Value());
		}
		else
		{
			LogError( "XLS WRAPPER > Unknown attribute \"%s\" in element \"%s\".\n", fileAttribute->Name(), xlsElement->Value() );
		}

		fileAttribute = fileAttribute->Next();
	}

	if (m_xlsFile.Load(fullFilePath))
	{

		TiXmlNode* pageNode = NULL;
		TiXmlElement* pageElement = NULL;

		pageNode = xlsElement->FirstChild("Page");
		while (pageNode != NULL)
		{
			pageElement = pageNode->ToElement();

			if (pageElement)
			{
				WrapPage(pageElement);
			}

			pageNode = xlsElement->IterateChildren("Page", pageNode);

		}

	}
	else
	{
		LogError( "XLS WRAPPER > Failed to open \"%s\". Is it open by an other app?!\n", fullFilePath );
	}
}

void XLSWrapper::WrapPage(TiXmlElement* pageElement)
{
	TiXmlAttribute* pageAttribute = pageElement->FirstAttribute();
	YExcel::BasicExcelWorksheet* currentSheet = NULL;
	int columnHeader = 0;
	char debugName[64] = {0};
	while(pageAttribute)
	{
		if (!strcasecmp(pageAttribute->Name(), "name"))
		{
			strcpy(debugName, pageAttribute->Value());
			currentSheet = m_xlsFile.GetWorksheet(pageAttribute->Value());
		}
		else if (!strcasecmp(pageAttribute->Name(), "columnHeader"))
		{
			columnHeader = pageAttribute->IntValue() - 1;
		}
		else
		{
			LogError("XLS WRAPPER > Unknown attribute \"%s\" in element \"%s\".\n", pageAttribute->Name(), pageElement->Value() );
		}

		pageAttribute = pageAttribute->Next();
	}

	if (currentSheet)
	{
		TiXmlNode* nodeNode = NULL;
		TiXmlElement* nodeElement = NULL;

		nodeNode = pageElement->FirstChild("Node");
		while (nodeNode != NULL)
		{
			nodeElement = nodeNode->ToElement();

			if (nodeElement)
			{
				CellNode currentNode;
				ReadWrapStructure(currentNode, nodeElement, currentSheet, columnHeader);
				WrapNode(&currentNode, currentSheet);
			}

			nodeNode = pageElement->IterateChildren("Node", nodeNode);
		}
	}
	else
	{
		LogError( "XLS WRAPPER > Unable to find sheet \"%s\"!\n", debugName );
	}

}

void XLSWrapper::WrapNode(CellNode* currentNode, YExcel::BasicExcelWorksheet* currentSheet)
{
	if (currentNode->m_file)
	{
		int i = currentNode->m_startLine;
		bool emptyLine = false;
		int currentColumn = currentNode->m_root.GetColumnID();
		while( i < currentSheet->GetTotalRows() && !emptyLine)
		{
			YExcel::BasicExcelCell* cell = currentSheet->Cell(i, currentColumn);
			TiXmlElement* wrapElement = NULL;

			// Empty raw breaker
			if (cell->Type() != YExcel::BasicExcelCell::UNDEFINED)
			{
				// ID
				if (cell->Type() == YExcel::BasicExcelCell::STRING)
				{
					wrapElement = currentNode->m_file->GetProperty(currentNode->m_root.GetProp().c_str(), cell->GetString());
				}
				else
				{
					wrapElement = currentNode->m_file->GetProperty(currentNode->m_root.GetProp().c_str(), cell->GetInteger());
				}

				if (wrapElement)
				{
					TiXmlNode* attributeNode = NULL;
					TiXmlElement* attributeElement = NULL;

					std::list<Element*>::iterator currentProperty = currentNode->m_properties.begin();
					for(; currentProperty != currentNode->m_properties.end(); ++currentProperty)
					{
						(*currentProperty)->Evaluate(currentSheet, i, wrapElement, (*currentProperty)->GetDuplicateID() );
					}
				}

			}
			else
			{
				emptyLine = true;
			}

			++i;
		}

	}

}

void XLSWrapper::ReadWrapStructure(CellNode& currentNode, TiXmlElement* nodeElement, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader)
{
	TiXmlAttribute* nodeAttribute = nodeElement->FirstAttribute();
	while(nodeAttribute)
	{
		if (!strcasecmp(nodeAttribute->Name(), "IDColumn"))
		{
			int columnID = GetColumnByID(nodeAttribute->Value(), currentSheet, columnHeader);
			if (columnID > -1)
			{
				currentNode.m_root.SetColumnID(columnID);
			}
			else
			{
				LogError( "XLS WRAPPER > Couldn't find column \"%s\"\n", nodeAttribute->Value());
			}
		}
		else if (!strcasecmp(nodeAttribute->Name(), "refFile"))
		{
			currentNode.m_file = GetFile(nodeAttribute->Value());
		}
		else if (!strcasecmp(nodeAttribute->Name(), "StartLine"))
		{
			currentNode.m_startLine = nodeAttribute->IntValue() - 1;
		}
		else if (!strcasecmp(nodeAttribute->Name(), "refClass"))
		{
			currentNode.m_root.SetProp(nodeAttribute->Value());
		}
		else
		{
			LogError( "XLS WRAPPER > Unknown attribute \"%s\" in element \"%s\".\n", nodeAttribute->Name(), nodeElement->Value() );
		}

		nodeAttribute = nodeAttribute->Next();
	}
	std::vector<int> index;
	AttributeStructureDispatcher(currentNode.m_properties, nodeElement, currentSheet, columnHeader, index);
}

void XLSWrapper::AttributeStructureDispatcher(std::list<Element*>& properties, TiXmlElement* nodeElement, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader, std::vector<int>& index)
{
	TiXmlNode* attributeNode = NULL;
	TiXmlElement* attributeElement = NULL;
	attributeNode = nodeElement->FirstChild();

	while (attributeNode != NULL)
	{
		attributeElement = attributeNode->ToElement();
		if (attributeElement)
		{
			if (!strcmp(attributeElement->Value(), "Attribute"))
			{
				ReadAttributeStructure(properties, attributeElement, currentSheet, columnHeader, index);
			}
			else if (!strcmp(attributeElement->Value(), "Switch"))
			{
				ReadSwitchStructure(properties, attributeElement, currentSheet, columnHeader, index);
			}
			else if (!strcmp(attributeElement->Value(), "Case"))
			{
				ReadCaseStructure(properties, attributeElement, currentSheet, columnHeader, index);
			}
			else
			{
				LogError("DATABASE ERROR > Unknown element \"%s\"\n", attributeElement->Value() );
			}
		}
		attributeNode = attributeNode->NextSibling();
	}
}

void XLSWrapper::ReadAttributeStructure(std::list<Element*>& properties, TiXmlElement* nodeElement, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader, std::vector<int>& index)
{
	bool isCollection = false;
	std::string prop;
	std::string subClass;
	TiXmlAttribute* nodeAttribute = nodeElement->FirstAttribute();
	std::vector<int> columnIDs;
	while (nodeAttribute)
	{
		if (!strcasecmp(nodeAttribute->Name(), "Column"))
		{
			std::string strCol = nodeAttribute->Value();
			isCollection = EvaluateColumnID(strCol, index, nodeAttribute);

			int columnID = 0;
			int dup = 1;
			bool found = false;
			while (columnID != -1)
			{
				columnID = GetColumnByID(strCol.c_str(), currentSheet, columnHeader, columnID, true, dup);
				if (columnID > -1)
				{
					columnIDs.push_back(columnID);
					++columnID;
					++dup;
					found = true;
				}
				else if (!found)
				{
					LogError( "XLS WRAPPER > Couldn't find column \"%s\"\n", strCol );
				}
			}
		}
		else if (!strcasecmp(nodeAttribute->Name(), "refProperty"))
		{
			prop = nodeAttribute->Value();
		}
		else if (!strcasecmp(nodeAttribute->Name(), "substitute"))
		{
			subClass = nodeAttribute->Value();
		}
		else
		{
			LogError( "XLS WRAPPER > Unknown attribute \"%s\" in element \"%s\".\n", nodeAttribute->Name(), nodeElement->Value() );
		}

		nodeAttribute = nodeAttribute->Next();
	}

	std::vector<int>::iterator currentCol = columnIDs.begin();
	int duplicate = 1;
	for (; currentCol != columnIDs.end(); ++currentCol, ++duplicate)
	{
		CellProperty* newProperty = new CellProperty();
		newProperty->SetColumnID(*currentCol);
		newProperty->SetProp(prop);
		newProperty->SetDuplicateID(duplicate);
		newProperty->SetSubstituteClass(subClass);

		if (isCollection)
		{
			index.push_back(duplicate);
		}
		AttributeStructureDispatcher(newProperty->GetElements(), nodeElement, currentSheet, columnHeader, index);
		if (isCollection)
		{
			index.pop_back();
		}
		properties.push_back(newProperty);
	}


}

void XLSWrapper::ReadSwitchStructure(std::list<Element*>& properties, TiXmlElement* nodeElement, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader, std::vector<int>& index)
{
	bool isCollection = false;
	TiXmlAttribute* nodeAttribute = nodeElement->FirstAttribute();
	std::vector<int> columnIDs;
	while (nodeAttribute)
	{
		if (!strcasecmp(nodeAttribute->Name(), "Cell"))
		{
			std::string strCol = nodeAttribute->Value();
			isCollection = EvaluateColumnID(strCol, index, nodeAttribute);

			int columnID = 0;
			int dup = 1;
			bool found = false;
			while (columnID != -1)
			{
				columnID = GetColumnByID(strCol.c_str(), currentSheet, columnHeader, columnID, true, dup);
				if (columnID > -1)
				{
					columnIDs.push_back(columnID);
					++columnID;
					++dup;
					found = true;
				}
				else if (!found)
				{
					LogError( "XLS WRAPPER > Couldn't find column \"%s\"\n", strCol );
				}
			}
		}
		else
		{
			LogError( "XLS WRAPPER > Unknown attribute \"%s\" in element \"%s\".\n", nodeAttribute->Name(), nodeElement->Value() );
		}

		nodeAttribute = nodeAttribute->Next();
	}

	std::vector<int>::iterator currentCol = columnIDs.begin();
	int duplicate = 1;
	for (; currentCol != columnIDs.end(); ++currentCol, ++duplicate)
	{
		Switch* newSwitch = new Switch();
		newSwitch->SetColumnID(*currentCol);
		newSwitch->SetDuplicateID(duplicate);
		if (isCollection)
		{
			index.push_back(duplicate);
		}
		AttributeStructureDispatcher(newSwitch->GetElements(), nodeElement, currentSheet, columnHeader, index);
		if (isCollection)
		{
			index.pop_back();
		}
		properties.push_back(newSwitch);
	}
}

void XLSWrapper::ReadCaseStructure(std::list<Element*>& properties, TiXmlElement* nodeElement, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader, std::vector<int>& index)
{
	TiXmlAttribute* nodeAttribute = nodeElement->FirstAttribute();
	Case* newCase = new Case();

	while (nodeAttribute)
	{
		if (!strcasecmp(nodeAttribute->Name(), "Value"))
		{
			newCase->SetValue(nodeAttribute->Value());
		}
		else
		{
			LogError( "XLS WRAPPER > Unknown attribute \"%s\" in element \"%s\".\n", nodeAttribute->Name(), nodeElement->Value() );
		}

		nodeAttribute = nodeAttribute->Next();
	}

	AttributeStructureDispatcher(newCase->GetElements(), nodeElement, currentSheet, columnHeader, index);
	properties.push_back(newCase);
	
}

int XLSWrapper::GetColumnByID(const char* columnID, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader, int startColumn, bool collection, int duplicate)
{
	int colCount = currentSheet->GetTotalCols();
	int i = startColumn;
	bool colFound = false;
	char ID[ID_LEN];
	strcpy(ID, columnID);
	stoupper(ID);
	while(i < colCount && !colFound)
	{
		YExcel::BasicExcelCell* cell = currentSheet->Cell(columnHeader, i);
		if (cell->Type() == YExcel::BasicExcelCell::STRING)
		{
			if (collection)
			{
				std::string strCol = columnID;
				if (strCol.find('#') != std::string::npos)
				{
					std::stringstream str;
					str << duplicate;

					strCol.replace(strCol.find('#'), 1, str.str());
					strcpy(ID, strCol.c_str());
					stoupper(ID);
				}
				
				char cellID[ID_LEN];
				strcpy(cellID, cell->GetString());
				stoupper(cellID);
				colFound = strcasecmp(cellID, ID) == 0;
			}
			else
			{
				colFound = !strcasecmp(cell->GetString(), columnID);
			}
		}

		++i;
	}

	if(!colFound)
	{
		if (startColumn == 0)
		{
			LogError( "XLS WRAPPER > Couldn't find column \"%s\"!\n", columnID );
		}
		return -1;
	}
	else
	{
		return i - 1;
	}
}

bool XLSWrapper::EvaluateColumnID(std::string& strCol, const std::vector<int>& index, const TiXmlAttribute* nodeAttribute)
{
	unsigned int indexDepth = 0;
	while (indexDepth < index.size() && strCol.find('#') != std::string::npos)
	{
		std::stringstream str;
		str << index[indexDepth];

		strCol.replace(strCol.find('#'), 1, str.str());
		++indexDepth;
	}
	if (indexDepth < index.size())
	{
		LogError( "XLS WRAPPER > Couldn't wrap all numbers on column \"%s\". Did you forget \"#\" ? %d are required.\n", nodeAttribute->Value(), index.size() );
	}

	bool isCollection = strCol.find('#') != std::string::npos;
	return isCollection;
}

DataBaseFile* XLSWrapper::GetFile(const char* id)
{
	std::list<DataBaseFile*>::iterator currentFiles = m_dbFiles.begin();
	for(; currentFiles != m_dbFiles.end(); ++currentFiles)
	{
		if (!strcasecmp((*currentFiles)->GetID().c_str(), id))
		{
			return *currentFiles;
		}
	}

	return NULL;
}
