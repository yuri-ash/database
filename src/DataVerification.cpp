#include "DataBase/DataVerification.h"
#include <assert.h>

DataVerification* DataVerification::m_dataVerifInstance = nullptr;

DataVerification::DataVerification()
{}

DataVerification::~DataVerification()
{}

DataVerification* DataVerification::GetDataVerification()
{
	if ( m_dataVerifInstance == nullptr )
	{
		m_dataVerifInstance = new DataVerification();
	}

	return m_dataVerifInstance;
}

bool DataVerification::Init( const std::string& file )
{
	m_validateState = luaL_newstate();
	luaL_openlibs( m_validateState );
	luaopen_DataBase( m_validateState );

	if ( luaL_dofile( m_validateState, file.c_str() ) != 0 )
	{
		std::cerr << "DataBaseError > Failed to load \"" << file << "\", with error: " << lua_tostring( m_validateState, -1 ) << std::endl;
		return false;
	}
	else
	{
		//std::cout << "Database > Open file \"" << file << "\"" << std::endl;
		//for ( auto customType : Property::GetCustomTypes() )
		//{
		//	std::string funcName = "validate_" + customType.GetName();
		//	lua_getglobal( m_validateState, funcName.c_str() );
		//	if ( lua_isfunction( m_validateState, -1 ) )
		//	{
		//		std::cout << funcName << " is a function" << std::endl;
		//	}
		//	else
		//	{
		//		std::cout << funcName << " is not a function" << std::endl;
		//	}
		//}

		//lua_pushstring( validateState, "" );
		//if ( lua_pcall( validateState, 1, 0, 0 ) != 0 )
		//{
		//	std::cerr << "DataBaseError > validation " << lua_tostring( validateState, -1 ) << std::endl;
		//	errs++;
		//}
		//lua_pop( validateState, 1 );
	}

	return true;
}

bool DataVerification::Deinit( )
{
	lua_close( m_validateState );

	return true;
}

bool DataVerification::IsValid( const std::string& typeName, char* value )
{
	std::string funcName = "validate_" + typeName;
	auto currentIndex = lua_gettop( m_validateState );
	lua_getglobal( m_validateState, funcName.c_str() );
	if ( lua_isfunction( m_validateState, -1 ) )
	{
		lua_pushstring( m_validateState, value );
		if ( lua_pcall( m_validateState, 1, 1, 0 ) != 0 )
		{
			std::cerr << "DataBaseError > validation " << lua_tostring( m_validateState, -1 ) << std::endl;
		}
		size_t len = 0;
		auto verifiedValue = lua_tolstring( m_validateState, -1, &len );
		strcpy( value, verifiedValue );
		lua_pop( m_validateState, 1 );
	}
	else
	{
		// No function to validate data, hence data is valid
		lua_pop( m_validateState, 1 );
	}
	assert( currentIndex == lua_gettop( m_validateState ) );
	return true;
}