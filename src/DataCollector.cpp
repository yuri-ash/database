#include "DataBase/DataCollector.h"
#include "DataBase/IssueReport.h"
#include "DataBase/Utils.h"
#include <assert.h>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>

#include "xxtea/xxtea.h"

#ifdef _WINDOWS
#include <Windows.h>
#endif

DataCollector::DataCollector()/*:
m_rootNode(NULL),
m_rootElement(NULL)*/
{
	m_currentClass = m_classes.begin();
	m_encryptKey[0] = '\0';
}

DataCollector::~DataCollector()
{
	Reset();
}

Class* DataCollector::AddNewClass()
{
	Class* newClass = new Class();
	m_classes.push_back( newClass );
	return newClass;
}

Data* DataCollector::AddNewData( Class* instance )
{
	Data* newData = new Data( instance );
	m_datas.push_back( newData );
	return newData;
}

bool DataCollector::OpenFile(const char* fileName)
{
	bool succeed = m_file.LoadFile(fileName);

	if (succeed)
	{
		//m_rootNode = m_file.FirstChild( "Data" );
		//assert( m_rootNode );
		//m_rootElement = m_rootNode->ToElement();
		//assert( m_rootElement );
	}
	return succeed;
}


void DataCollector::AddToPropertyIgnoreList( const std::string& propertyName )
{
	m_propertyIgnoredList.push_back( propertyName );
}

bool DataCollector::IsPropertyIgnored( const std::string& propertyName )
{
	for ( const auto& propertyIgnored : m_propertyIgnoredList )
	{
		if ( propertyIgnored == propertyName )
		{
			return true;
		}
	}
	return false;
}

void DataCollector::Reset()
{
	std::list<Class*>::iterator currentClass = m_classes.begin();
	for(; currentClass != m_classes.end(); ++currentClass)
	{
		delete *currentClass;
	}
	m_classes.clear();

	std::list<Data*>::iterator currentData = m_datas.begin();
	for(; currentData != m_datas.end(); ++currentData)
	{
		delete *currentData;
	}
	m_datas.clear();

	std::list<char*>::iterator currentFileName = m_filesDone.begin();
	for(; currentFileName != m_filesDone.end(); ++currentFileName)
	{
		delete *currentFileName;
	}
	m_filesDone.clear();

}

int DataCollector::QueryExternIDs(const char* filePath)
{
	//errors accumulator
	const char* pathEnd = strrchr(filePath, '\\');
	if (!pathEnd)
	{
		pathEnd = strrchr(filePath, '/');
	}
	if (!pathEnd)
	{
		strcpy(m_currentFolder, "./");
		QueryExternIDsInFile(filePath);
	}
	else
	{
        strncpy(m_currentFolder, filePath, static_cast<size_t>(pathEnd - filePath));
		m_currentFolder[pathEnd - filePath] = '\0';
        int directoryLen = static_cast<int>(strlen(m_currentFolder));
		if (m_currentFolder[directoryLen] != '\\' && m_currentFolder[directoryLen] != '/')
		{
			strcat(m_currentFolder, "/");
		}
		char fileName[64] = {0};
        strcpy(fileName, filePath + static_cast<int>(pathEnd - filePath) + 1);
		QueryExternIDsInFile(fileName);
	}

	return 0;
}


int DataCollector::QueryExternIDsInFile(const char* fileName)
{
	//errors accumulator
	std::list<char*>::iterator currentFileName = m_filesDone.begin();
	while(currentFileName != m_filesDone.end() && strcmp(*currentFileName, fileName))
	{
		++currentFileName;
	}

	if (currentFileName == m_filesDone.end())
	{
		char* newFileValue = new char[strlen(fileName) + 1];
		strcpy(newFileValue, fileName);
		m_filesDone.push_back(newFileValue);
		char fullPath[512] = {0};
		strcpy(fullPath, m_currentFolder);
		strcat(fullPath, fileName);

		std::ifstream currentFile(fullPath);

		const char* extension = strrchr(fileName, '.');

		if (currentFile.is_open())
		{
			if (extension && !strcasecmp(extension, ".cs"))
			{
				SearchForIDsCS(&currentFile);
			}
			else
			{
				SearchForIDs(&currentFile);
			}
		}
		else
		{
			//std::cerr << "DataBase ERROR > Failed to open extern ID file \"" << fullPath << "\"!\n";
		}

		currentFile.close();
	}
	return 0;
}

int DataCollector::SearchForIDsCS(std::ifstream* currentFile)
{
	//errors accumulator
	bool skipNextLine = false;
	int lineCount = 0;
	while(!currentFile->eof())
	{
		char line[2048] = {0};
		currentFile->getline(line, 2048);
		++lineCount;
		if (!skipNextLine)
		{
			int wordsPosition[16];
			int wordsCount = CutLine(line, wordsPosition, 16);

			if (wordsCount > 0)
			{
				if (!strcmp(&line[wordsPosition[0]], "enum") || wordsCount > 1 && !strcmp(&line[wordsPosition[1]], "enum"))
				{
					ParseEnum(currentFile);
				}
				else
				{
					for(int i = 0; i < wordsCount; ++i)
					{
						if (!strcmp(&line[wordsPosition[i]], "="))
						{
							AddExternIDs(&line[wordsPosition[i - 1]], atoi(&line[wordsPosition[i + 1]]));
						}
					}
				}
			}
		}
		else
		{
			skipNextLine = false;
		}
	}
	return 0;
}

int DataCollector::SearchForIDs(std::ifstream* currentFile)
{
	bool skipNextLine = false;
	int lineCount = 0;
	while(!currentFile->eof())
	{
		char line[2048] = {0};
		currentFile->getline(line, 2048);
		++lineCount;
		if (!skipNextLine)
		{
			int wordsPosition[16];
			int wordsCount = CutLine(line, wordsPosition, 16);

			if (wordsCount > 0)
			{
				if (!strcmp(&line[wordsPosition[0]], "#define"))
				{
					if (wordsCount == 3)
					{
						AddExternIDs(&line[wordsPosition[1]], atoi(&line[wordsPosition[2]]));
					}
				}
				else if (!strcmp(&line[wordsPosition[0]], "#include"))
				{
					QueryExternIDsInFile(&line[wordsPosition[1]]);
				}
				else if (!strcmp(&line[wordsPosition[0]], "#ifndef"))
				{
					skipNextLine = true;
				}
				else if ((!strcmp(&line[wordsPosition[0]], "typedef") && wordsCount > 1 && !strcmp(&line[wordsPosition[1]], "enum"))
					|| (!strcmp(&line[wordsPosition[0]], "enum") )
					)
				{
					ParseEnum(currentFile);
				}
				else if (!strcmp(&line[wordsPosition[0]], "final") && !strcmp(&line[wordsPosition[1]], "static"))
				{
					if (!strcmp(&line[wordsPosition[2]], "int")
						|| !strcmp(&line[wordsPosition[2]], "short")
						|| !strcmp(&line[wordsPosition[2]], "byte"))
					{
						if (!strcmp(&line[wordsPosition[4]], "="))
						{
							AddExternIDs(&line[wordsPosition[3]], atoi(&line[wordsPosition[5]]));
						}
					}
				}
				else if (!strcmp(&line[wordsPosition[1]], "final") && !strcmp(&line[wordsPosition[2]], "static"))
				{
					if (!strcmp(&line[wordsPosition[3]], "int")
						|| !strcmp(&line[wordsPosition[3]], "short")
						|| !strcmp(&line[wordsPosition[3]], "byte"))
					{
						if (!strcmp(&line[wordsPosition[5]], "="))
						{
							AddExternIDs(&line[wordsPosition[4]], atoi(&line[wordsPosition[6]]));
						}
					}
				}
				else if(wordsCount == 3)
				{
					if (!strcmp(&line[wordsPosition[1]], "="))
					{
						AddExternIDs(&line[wordsPosition[0]], atoi(&line[wordsPosition[2]]));
					}
				}
			}
		}
		else
		{
			skipNextLine = false;
		}
	}
	return 0;
}

int DataCollector::AddExternIDs(const char* stringID, const int intID)
{
	std::map<std::string, int, string_less>::iterator it = m_externIDs.find(stringID);

	if (it != m_externIDs.end())
	{
		LogWarning( "Duplicate extern ID found! \"%s\" is already referenced.\n", stringID);
		return 0;//1 error happened
	}

	m_externIDs.insert(std::make_pair(std::string(stringID), intID));


	//std::list<ExternId>::iterator currentID = m_externIDs.begin();
	//for(; currentID != m_externIDs.end(); ++currentID)
	//{
	//	if (!strcasecmp((*currentID).m_stringID, stringID))
	//	{
	//		std::cerr << "DataBase ERROR > Duplicate extern ID found! \"" << stringID << "\" is already referenced.\n";
	//		return;
	//	}
	//}

	//ExternId newID;
	//strcpy(newID.m_stringID, stringID);
	//newID.m_intID = intID;
	//m_externIDs.push_back(newID);

	return 0;// no error
}

int DataCollector::ParseEnum(std::ifstream* currentFile)
{
	//error accumulator
	bool inEnum = true;
	int currentEnumValue = 0;
	while(!currentFile->eof() && inEnum)
	{
		char line[1024] = {0};
		currentFile->getline(line, 1024);
		int wordsPosition[16];
		int wordsCount = CutLine(line, wordsPosition, 16, "\n \t,");

		if (wordsCount > 0)
		{
			if (!strcmp(&line[wordsPosition[0]], "{")
				|| line[wordsPosition[0]] == '/')
			{
				continue;
			}
			else if (strchr(&line[wordsPosition[0]], '}'))
			{
				inEnum = false;
			}
			else
			{
				if (wordsCount > 4)
				{
					currentEnumValue = atoi(&line[wordsPosition[2]]);
					if (!strcmp(&line[wordsPosition[3]], "<<"))
					{
						currentEnumValue <<= atoi(&line[wordsPosition[4]]);
					}
					else if (!strcmp(&line[wordsPosition[3]], ">>"))
					{
						currentEnumValue >>= atoi(&line[wordsPosition[4]]);
					}
				}
				else if (wordsCount > 2)
				{
					currentEnumValue = atoi(&line[wordsPosition[2]]);
				}
				AddExternIDs(&line[wordsPosition[0]], currentEnumValue);
				++currentEnumValue;
			}
		}
	}
	return 0;
}

int DataCollector::ResolveExternIDs(const char* externID)
{
	if (externID)
	{
		std::map<std::string, int, string_less>::iterator it = m_externIDs.find(externID);
		if (it != m_externIDs.end())
			return it->second;

		//std::list<ExternId>::iterator currentID = m_externIDs.begin();
		//for(; currentID != m_externIDs.end(); ++currentID)
		//{
		//	if (!strcasecmp((*currentID).m_stringID, externID))
		//	{
		//		return (*currentID).m_intID;
		//	}
		//}

		if (!strcasecmp(externID, "0") || !strcasecmp(externID, "NULL"))
		{
			return 0;
		}
	}

	std::cerr << "DataBase ERROR > Can't find extern ID \"" << externID << "\"!\n";
	return -1;
}

//int DataCollector::ResolveInternIDs(const char* internID, const char* className)
//{
//	std::list<Data*>::iterator currentData = m_datas.begin();
//	for(; currentData != m_datas.end(); ++currentData)
//	{
//		if (!strcasecmp((*currentData)->GetClass()->GetName().c_str(), className) && !strcasecmp((*currentData)->GetID()->m_stringID.c_str(), internID))
//		{
//			return (*currentData)->GetID()->m_intID;
//		}
//	}
//
//	std::cerr << "DataBase ERROR > Can't find intern ID \"" << internID << "\" in class \"" << className <<"\"!\n";
//	return -1;
//}

int DataCollector::ResolveDependanceIDs()
{
	std::list<Data*>::iterator currentData = m_datas.begin();
	for(; currentData != m_datas.end(); ++currentData)
	{
		ResolveDependanceIDs((*currentData));
	}
	return 0;
}

int DataCollector::ResolveDependanceIDs(Data* data)
{
	if (data->GetParentData())
	{
		ResolveDependanceIDs(data->GetParentData());
	}

	std::map<const Property*, Data::DataValue>::iterator currentSubData = data->GetSubData().begin();
	for(; currentSubData != data->GetSubData().end(); ++currentSubData)
	{
		if (currentSubData->first->GetType() == Property::PT_CLASS)
		{
			std::list<Data::DataValue::DataChunk>::iterator currentID = (*currentSubData).second.m_values.begin();
			for(; currentID != (*currentSubData).second.m_values.end(); ++currentID)
			{
				if ( (*currentID).m_dataString == "null" )
				{
					continue;
				}
				else if (!(*currentID).m_dataID)
				{
					Class* newClass = nullptr;
					GetClass((*currentSubData).first->GetTypeString(), newClass);
					assert( newClass != nullptr );
					if ( newClass != nullptr )
					{
						(*currentID).m_dataID = newClass->GetID( (*currentID).m_dataString );
						if ( (*currentID).m_dataID == NULL && IsNumber( (*currentID).m_dataString.c_str(), false ) )
						{
							GetClass( (*currentSubData).first->GetTypeString(), newClass );
							(*currentID).m_dataID = newClass->GetID( atoi( (*currentID).m_dataString.c_str() ) );
							std::cerr << "DataBase WARNING > Using explicit ID \"" << (*currentID).m_dataString << "\" of class \"" << (*currentSubData).first->GetTypeString() << "\"! Unsafe operation, thanks to use litteral IDs.\n";
						}
						if ( (*currentID).m_dataID == NULL || (*currentID).m_dataID->m_intID == -1 )
						{
							LogCriticalError( "DataBase ERROR > Can't find ID \"%s\" of class \"%s\"!\n", (*currentID).m_dataString.c_str(), (*currentSubData).first->GetTypeString().c_str() );
						}
					}
				}
			}
		}
	}
	return 0;
}

int DataCollector::ResolveParenting()
{
	std::list<Class*>::iterator currentClass = m_classes.begin();
	for(; currentClass != m_classes.end(); ++currentClass)
	{
		if ((*currentClass)->GetParentName() != "")
		{
			Class* parentClass;
			GetClass((*currentClass)->GetParentName(), parentClass);
			if (parentClass != NULL)
			{
				(*currentClass)->SetParent(parentClass);
			}
			else
			{
				LogCriticalError( "DataBase ERROR > Can't find class \"%s\" parent of class \"%s\"!\n", (*currentClass)->GetParentName(), (*currentClass)->GetName() );
			}
		}
	}
	return 0;
}

void DataCollector::GetDerivedClasses( std::list<Class*>& derivedClasses, Class* parentClass)
{
    for(auto currentClass : m_classes)
    {
        if ( currentClass->IsParent(parentClass, true) )
        {
            derivedClasses.push_back( currentClass );
        }
    }
}

int DataCollector::ReadCustomType()
{
	if (!m_file.Error())
	{
		TiXmlNode* rootNode = NULL;
		TiXmlElement* rootElement = NULL;
		TiXmlNode* typeNode = NULL;
		TiXmlElement* typeElement = NULL;

		rootNode = m_file.FirstChild("Define");
		assert(rootNode);
		rootElement = rootNode->ToElement();
		assert(rootElement);


		typeNode = rootElement->FirstChild("Type");
		while (typeNode != NULL)
		{
			typeElement = typeNode->ToElement();

			if (typeElement)
			{
				ReadCustomType(typeElement);
			}

			typeNode = typeNode->NextSibling();

		}
	}
	return 0;
}

int DataCollector::ReadCustomType(TiXmlElement* typeElement)
{
	CustomType newType;
	TiXmlAttribute* attribute = typeElement->FirstAttribute();

	while(attribute)
	{
		if (!strcasecmp(attribute->Name(), "name"))
		{
			newType.SetName(attribute->Value());
		}
		else if (!strcasecmp(attribute->Name(), "alias"))
		{
			newType.SetAlias(attribute->Value());
		}
		else if ( !strcasecmp( attribute->Name(), "size" ) )
		{
			newType.SetSize( atoi( attribute->Value() ) );
		}
		else if ( !strcasecmp( attribute->Name(), "array" ) )
		{
			newType.SetArray( !strcasecmp( attribute->Value(), "true" ) );
		}
		else if (!strcasecmp(attribute->Name(), "archetype"))
		{
			newType.SetArchetype(attribute->Value());
		}
		else
		{
			LogWarning( "Unknown attribute \"%s\"!\n", attribute->Name() );
		}
		attribute = attribute->Next();
	}

	Property::AddCustomType(newType);
	return 0;
}

int DataCollector::ResolveOperation(const char* formula)
{
    int formulaLen = static_cast<int>(strlen(formula));
	char buffer[2048];
	memset(buffer, 0, 2048);
	int currentBufferPos = 0;
	int currentResult = 0;
	char currentOperator = 0;
	std::vector<int> operandStack;
	std::vector<char> operatorStack;
	for(int i = 0; i < formulaLen; ++i)
	{
		switch(formula[i])
		{
		case '+':
			if (buffer[0] != 0)
				currentResult = Operate(currentResult, currentOperator, ResolveValue(buffer));
			currentOperator = '+';
			memset(buffer, 0, 2048);
			currentBufferPos = 0;
			break;
		case '-':
			if (buffer[0] != 0)
				currentResult = Operate(currentResult, currentOperator, ResolveValue(buffer));
			currentOperator = '-';
			memset(buffer, 0, 2048);
			currentBufferPos = 0;
			break;
		case '*':
			if (buffer[0] != 0)
				currentResult = Operate(currentResult, currentOperator, ResolveValue(buffer));
			currentOperator = '*';
			memset(buffer, 0, 2048);
			currentBufferPos = 0;
			break;
		case '/':
			if (buffer[0] != 0)
				currentResult = Operate(currentResult, currentOperator, ResolveValue(buffer));
			currentOperator = '/';
			memset(buffer, 0, 2048);
			currentBufferPos = 0;
			break;
		case '&':
			if (buffer[0] != 0)
				currentResult = Operate(currentResult, currentOperator, ResolveValue(buffer));
			currentOperator = '&';
			memset(buffer, 0, 2048);
			currentBufferPos = 0;
			break;
		case '|':
			if (buffer[0] != 0)
				currentResult = Operate(currentResult, currentOperator, ResolveValue(buffer));
			currentOperator = '|';
			memset(buffer, 0, 2048);
			currentBufferPos = 0;
			break;
		case '(':
			operandStack.push_back(currentResult);
			operatorStack.push_back(currentOperator);
			break;
		case ')':
			if (buffer[0] != 0)
				currentResult = Operate(operandStack[operandStack.size() - 1], operatorStack[operatorStack.size() - 1], currentResult);
			memset(buffer, 0, 2048);
			currentBufferPos = 0;
			operandStack.pop_back();
			operatorStack.pop_back();
			break;
		case '\t':
		case ' ':
			break;
		default:
			buffer[currentBufferPos] = formula[i];
			++currentBufferPos;
			break;
		}
	}

	return Operate(currentResult, currentOperator, ResolveValue(buffer));
}

int DataCollector::ResolveValue(const char* operand)
{
	int value = static_cast<int>(atoll(operand));
	if (value == 0 && operand[0] != '0')
	{
		value = ResolveExternIDs(operand);
	}
	return value;
}

int DataCollector::Operate(int operand1, char op, int operand2)
{
	switch(op)
	{
	case '+':
		return operand1 + operand2;
	case '-':
		return operand1 - operand2;
	case '*':
		return operand1 * operand2;
	case '/':
		return operand1 / operand2;
	case '&':
		return operand1 & operand2;
	case '|':
		return operand1 | operand2;
	case 0:
		return operand2;
	}
	return operand1;
}

int DataCollector::GetClass(const std::string& className, Class*& newClass)
{
	bool found = false;
	newClass = nullptr;
	std::list<Class*>::iterator currentClass = m_classes.begin();

	for(; currentClass != m_classes.end(); ++currentClass)
	{
		if (!strcasecmp((*currentClass)->GetName().c_str(), className.c_str()))
		{
			newClass = *currentClass;
			found = true;
			break;
		}
	}

	if (!found)
	{
		LogCriticalError( "DataBase ERROR > Can't find class \"%s\"!\n", className.c_str() );
	}
	return 0;
}

int DataCollector::WriteData(const char* destination)
{
	std::list<Class*>::iterator currentClass = m_classes.begin();
	for(; currentClass != m_classes.end(); ++currentClass)
	{
		(*currentClass)->ValidateIDs();

		char fullPath[MAX_PATH_LEN] = {0};
		strcpy(fullPath, destination);
		if (fullPath[strlen(destination) - 1] != '/' && fullPath[strlen(destination) - 1] != '\\' )
		{
			strcat(fullPath, "/");
		}
		strcat(fullPath, (*currentClass)->GetObfuscateName().c_str());
		strcat(fullPath, ".db");

#ifdef _WINDOWS
        CreateDirectory(destination, NULL);
#else //_WINDOWS
        int status;
        status = mkdir(destination, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#endif //_WINDOWS


		int currentSize = 4096;
		char* fullBuffer = new char[currentSize];
		int currentOffset = 0;

		currentOffset = WriteDataHeader(fullBuffer, currentOffset, (*currentClass));
		std::list<Data*>::iterator currentData = m_datas.begin();

		for(; currentData != m_datas.end(); ++currentData)
		{
			if ((*currentData)->GetClass()->GetName() == (*currentClass)->GetName())
			{
				char* buffer;
				int size = (*currentData)->GetDataBuffer(buffer);

				if (size + currentOffset > currentSize)
				{
					currentSize += 4096 * ((size + currentOffset - currentSize) / 4096 + 1);
					char* newBuffer = new char[currentSize];
					memcpy(newBuffer, fullBuffer, currentOffset);

					delete[] fullBuffer;
					fullBuffer = newBuffer;
				}

				memcpy(fullBuffer + currentOffset, buffer, size);
				currentOffset += size;
			}
		}

		xxtea_long sizeRet = currentOffset;
		unsigned char* encryptBuffer = reinterpret_cast<unsigned char*>(fullBuffer);

		if ( strlen( m_encryptKey ) > 0)
		{
            encryptBuffer = xxtea_encrypt((unsigned char*)fullBuffer, static_cast<xxtea_long>(currentOffset), reinterpret_cast<unsigned char*>(m_encryptKey), static_cast<xxtea_long>(strlen(m_encryptKey)), &sizeRet);
			//unsigned char* decryptBuffer = xxtea_decrypt((unsigned char*)encryptBuffer, sizeRet, (unsigned char*)(key), strlen(key), &sizeRet);
		}

		std::ofstream outputFile(fullPath, std::ios::binary);
		if (outputFile.is_open())
		{
			outputFile.write((char*)encryptBuffer, sizeRet);
		}
		else
		{
			std::cerr << "Couldn't write file " << fullPath << std::endl;
		}
		outputFile.close();
	}
	return 0;
}

int DataCollector::WriteDataHeader(char* buffer, int offset, const Class* classWrite)
{
	unsigned int dataCount = 0;

	// Real data count
    dataCount = static_cast<unsigned int>(classWrite->GetIDs().size());
	memcpy(buffer + offset, &dataCount, sizeof(dataCount));
	offset += sizeof(dataCount);

	// File data count
	dataCount = GetClassDataCount(classWrite->GetName());
	memcpy(buffer + offset, &dataCount, sizeof(dataCount));
	offset += sizeof(dataCount);

	return offset;
}

unsigned int DataCollector::GetClassDataCount(const std::string& className)
{
	unsigned int currentCount = 0;
	std::list<Data*>::iterator currentData = m_datas.begin();
	for(; currentData != m_datas.end(); ++currentData)
	{
		if ((*currentData)->GetClass()->GetName() == className)
		{
			++currentCount;
		}
	}

	return currentCount;
}

int DataCollector::GetData(Class* classTarget, const char* ID, Data*& newData )
{
	bool found = false;
	std::list<Data*>::iterator currentData = m_datas.begin();

	for(; currentData != m_datas.end(); ++currentData)
	{
		if ((*currentData)->GetClass()->GetName() == classTarget->GetName()
			&& !strcasecmp((*currentData)->GetID()->m_stringID.c_str(), ID))
		{
			newData = *currentData;
			found = true;
			break;
		}
	}

	if (!found)
	{
		std::cerr << "DataBase ERROR > Can't find ID \"" << ID << "\" of class \"" << classTarget->GetName() << "\"!\n";
		return 1; //not found -> 1 error happened
	}
	return 0;
}

void DataCollector::SetEncryptKey(const char* key)
{
	strncpy(m_encryptKey, key, 16);
	m_encryptKey[16] = '\0';
}

const char* DataCollector::GetEncryptKey()
{
	return m_encryptKey;
}

