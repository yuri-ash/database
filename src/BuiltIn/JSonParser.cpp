#include "DataBase/BuiltIn/JSonParser.h"
#include "DataBase/DataCollector.h"
#include "DataBase/Property.h"
#include "nlohmann/json.hpp"
#include <fstream>
#include <sstream>

using json = nlohmann::json;

JSONParser::JSONParser()
{}

JSONParser::~JSONParser()
{}

bool JSONParser::CanReadExtension( const std::string& extension ) const
{
	return extension == "json";
}

bool JSONParser::ParseDefinitionFile( const std::string& fileName, DataCollector* collector )
{
	std::ifstream defFile( fileName );
	if ( defFile.is_open() )
	{
		json classDef;
		// load from file
		defFile >> classDef;

		for ( auto jsonStruct : classDef.items() )
		{
			auto key = jsonStruct.key();
			if ( strcasecmp( key.c_str(), "Definition" ) == 0 )
			{
				ParseClassesDefinition( jsonStruct.value(), collector );
			}
			else
			{
				std::cout << "Unsupported key \""<< key<< "\" in json "<< fileName << std::endl;
			}
		}
	}

	defFile.close();
	return true;
}

bool JSONParser::ParseDataFile( const std::string& fileName, DataCollector* collector )
{
	std::ifstream dataFile( fileName );
	if ( dataFile.is_open() )
	{
		json dataDef;
		// load from file
		dataFile >> dataDef;

		for ( auto jsonStruct : dataDef.items() )
		{
			auto key = jsonStruct.key();
			if ( strcasecmp( key.c_str(), "Data" ) == 0 )
			{
				ParseDataDefinition( jsonStruct.value(), collector );
			}
			else if( !collector->IsPropertyIgnored( key.c_str() ) )
			{
				std::cout << "Unsupported key \"" << key << "\" in json " << fileName << std::endl;
			}
		}
	}

	dataFile.close();
	return true;
}

bool JSONParser::SetupIgnoreList( const std::string& fileName, DataCollector* collector )
{
	std::ifstream ignoreFile( fileName );
	if ( ignoreFile.is_open() )
	{
		json dataDef;
		// load from file
		ignoreFile >> dataDef;

		for ( auto jsonStruct : dataDef.items() )
		{
			auto key = jsonStruct.key();
			if ( strcasecmp( key.c_str(), "Property" ) == 0 )
			{
				assert( jsonStruct.value().is_array() );
				if ( !jsonStruct.value().is_array() )
				{
					return false;
				}

				for ( auto value : jsonStruct.value() )
				{
					collector->AddToPropertyIgnoreList( value.get<std::string>() );
				}
			}
			else
			{
				std::cout << "Unsupported key \"" << key << "\" in json " << fileName << std::endl;
			}
		}
	}

	ignoreFile.close();
	return true;
}

bool JSONParser::ParseClassesDefinition( const json& jsonDef, DataCollector* collector )
{
	for ( auto jsonStruct : jsonDef.items() )
	{
		auto key = jsonStruct.key();
		if ( strcasecmp( key.c_str(), "Classes" ) == 0 )
		{
			if ( jsonStruct.value().is_array() )
			{
				for ( auto classDef : jsonStruct.value() )
				{
					ParseClassDefinition( classDef, collector );
				}
			}
			else
			{
				std::cerr << "Unexpected value at key \"" << key << ". Array expected, got \""<< jsonStruct.value().type_name() << "\"" << std::endl;
			}
		}
		else
		{
			std::cout << "Unsupported key \"" << key << "\"" << std::endl;
		}
	}

	return true;
}

bool JSONParser::ParseClassDefinition( const json& jsonDef, DataCollector* collector )
{
	Class* newClass = collector->AddNewClass();

	for ( auto jsonStruct : jsonDef.items() )
	{
		auto key = jsonStruct.key();
		if ( strcasecmp( key.c_str(), "Attributes" ) == 0 )
		{
			ParseClassAttributes( jsonStruct.value(), newClass );
		}
		else if ( strcasecmp( key.c_str(), "Properties" ) == 0 )
		{
			ParseClassProperties( jsonStruct.value(), newClass, collector );
		}
		else
		{
			std::cout << "Unsupported key \"" << key << "\"" << std::endl;
		}
	}
	newClass->CreatePadding();
	return true;
}

bool JSONParser::ParseClassAttributes( const json& jsonDef, Class* newClass )
{
	for ( auto jsonStruct : jsonDef.items() )
	{
		auto key = jsonStruct.key();
		if ( strcasecmp( key.c_str(), "Name" ) == 0 )
		{
			newClass->SetName( jsonStruct.value() );
		}
		else if ( strcasecmp( key.c_str(), "Parent" ) == 0 )
		{
			newClass->SetParentName( jsonStruct.value() );
		}
		else
		{
			std::cout << "Unsupported key \"" << key << "\"" << std::endl;
		}
	}
	return true;
}

bool JSONParser::ParseClassProperties( const json& jsonDef, Class* newClass, DataCollector* collector )
{
	if ( jsonDef.is_array() )
	{
		for ( auto properties : jsonDef )
		{
			Property newProperty;
			for ( auto property : properties.items() )
			{
				auto key = property.key();
				if ( strcasecmp( key.c_str(), "Name" ) == 0 )
				{
					newProperty.SetName( property.value() );
				}
				else if ( strcasecmp( key.c_str(), "Type" ) == 0 )
				{
					newProperty.SetType( property.value().get<const std::string>().c_str() );
				}
				else if ( strcasecmp( key.c_str(), "Collection" ) == 0 )
				{
					newProperty.SetCollection( property.value() );
				}
				else if ( strcasecmp( key.c_str(), "Default" ) == 0 )
				{
					newProperty.SetDefaultValue( property.value() );
				}
				else
				{
					std::cout << "Unsupported key \"" << key << "\"" << std::endl;
				}
			}
            assert( !collector->IsPropertyIgnored( newProperty.GetName() ) ); //"Trying to add a property that has been ignored."
			newClass->AddProperty( newProperty );
		}
	}
	else
	{
		std::cerr << "Unexpected value for properties. Array expected, got \"" << jsonDef.type_name() << "\"" << std::endl;
		return false;
	}
	return true;
}

bool JSONParser::ParseDataDefinition( const json& jsonDef, DataCollector* collector )
{
	if ( jsonDef.is_array() )
	{
		for ( auto data : jsonDef )
		{
			for ( auto dataClass : data.items() )
			{
				Class* readClass = nullptr;
				collector->GetClass( dataClass.key(), readClass );
				if ( readClass != nullptr )
				{
					// Unfortunate dummy we need to keep as data may need to be retrieved from different source.
					Data* newData = nullptr;
					ParseDataClass( dataClass.value(), readClass, newData, collector );
				}
				else
				{
					std::cerr << "Could not find class named \"" << dataClass.key() << "\" while parsing data." << std::endl;
				}
			}
		}
	}
	else
	{
		std::cerr << "Unexpected value for properties. Array expected, got \"" << jsonDef.type_name() << "\"" << std::endl;
	}

	return true;
}

bool JSONParser::ParseDataClass( const json& jsonDef, Class* dataClass, Data* &newData, DataCollector* collector )
{
	Attributes dataAttributes;
	memset( &dataAttributes, 0, sizeof( dataAttributes ) );
	dataAttributes.m_overrideClass = dataClass;

	if ( jsonDef.contains( "attributes" ) )
	{
		auto attributes = jsonDef["attributes"];
		ParseDataAttributes( dataAttributes, attributes, dataClass, collector );
	}

	newData = collector->AddNewData( dataAttributes.m_overrideClass );

	if ( !dataAttributes.m_ID.empty() )
	{
		newData->SetID( dataAttributes.m_ID.c_str() );
	}

	for ( auto jsonStruct : jsonDef.items() )
	{
		auto key = jsonStruct.key();
		if ( strcasecmp( key.c_str(), "Attributes" ) == 0 )
		{
			// already taken care of
		}
		// Assume property, let the function decide if its a misfits
		else
		{
			if ( jsonStruct.value().is_array() )
			{
				for ( auto propertyValue : jsonStruct.value() )
				{
					ParseDataProperty( propertyValue, key, dataAttributes.m_overrideClass, newData, collector );
				}
			}
			else
			{
				ParseDataProperty( jsonStruct.value(), key, dataAttributes.m_overrideClass, newData, collector );
			}
		}
	}
	return true;
}

bool JSONParser::ParseDataAttributes( Attributes &dataAttibutes, const json& jsonDef, Class* &currentClass, DataCollector* collector )
{
	for ( auto jsonStruct : jsonDef.items() )
	{
		auto key = jsonStruct.key();
		if ( strcasecmp( key.c_str(), "ID" ) == 0 )
		{
			dataAttibutes.m_ID = jsonStruct.value().get<std::string>();
		}
		else if ( strcasecmp( key.c_str(), "class" ) == 0 )
		{
			Class* substitute;
			auto className = jsonStruct.value().get<std::string>();
			if ( className == currentClass->GetName() )
			{
				// Trying to substitute with same class, no need
				continue;
			}
			collector->GetClass( className.c_str(), substitute );
			if ( substitute )
			{
				if ( substitute->IsParent( currentClass, true ) )
				{
					dataAttibutes.m_overrideClass = substitute;
				}
				else
				{
					std::cerr << "DataBase ERROR > Class \"" << className << "\" is not affiliated to class \"" << currentClass->GetName() << "\"!\n";
					return false;
				}
			}
			else
			{
				std::cerr << "DataBase ERROR > Can't find class named \"" << className << "\" substitute of \"" << currentClass->GetName() << "\"!\n";
				return false;
			}
		}
		else
		{
			std::cout << "Unsupported key \"" << key << "\"" << std::endl;
		}
	}
	return true;
}

bool JSONParser::ParseDataProperty( const json& jsonDef, const std::string& propertyName, Class* currentClass, Data* currentData, DataCollector* collector )
{
	const Property* currentProp = currentClass->GetProperty( propertyName );
	if ( currentProp != nullptr )
	{
		if ( jsonDef.is_object() && currentProp->GetType() == Property::PT_CLASS )
		{
			Class* propertyClass;
			collector->GetClass( currentProp->GetTypeString(), propertyClass );
			if ( propertyClass )
			{
				Data* subData;
				ParseDataClass( jsonDef, propertyClass, subData, collector );
				ClassID* newClassID;
				subData->GetIDByClass( currentProp->GetTypeString(), newClassID );
				if ( newClassID == nullptr )
				{
					// No ID for this instance? make one
					subData->ForceDefaultID();
					subData->GetIDByClass( currentProp->GetTypeString(), newClassID );
					assert( newClassID != nullptr );
				}
				currentData->PushSubData( newClassID, currentProp );
			}
			else
			{
				std::cerr << "Couldn't find class \"" << currentProp->GetTypeString() << "\" for property \"" << propertyName << "\"." << std::endl;
			}
		}
		else
		{
			if ( jsonDef.is_string() )
			{
				currentData->PushSubData( currentProp, jsonDef.get<std::string>().c_str() );
			}
			else
			{
				std::stringstream ss;
				ss << jsonDef;
				currentData->PushSubData( currentProp, ss.str().c_str() );
			}
		}
	}
	else if ( currentClass->GetParent() )
	{
		return ParseDataProperty( jsonDef, propertyName, currentClass->GetParent(), currentData->GetParentData(), collector );
	}
	else if ( !collector->IsPropertyIgnored( propertyName ) )
	{
		std::cerr << " Unknown property \"" << propertyName << "\" in class \"" << currentClass->GetName() << "\"." << std::endl;
		return false;
	}

	return true;
}
