#include "DataBase/BuiltIn/XMLParser.h"
#include "DataBase/Class.h"
#include "DataBase/Data.h"
#include "DataBase/DataCollector.h"

#include "tinyxml/tinyxml.h"

XMLParser::XMLParser()
{}

XMLParser::~XMLParser()
{}

bool XMLParser::CanReadExtension( const std::string& extension ) const
{
	return extension == "xml";
}

bool XMLParser::ParseDefinitionFile( const std::string& fileName, DataCollector* collector )
{
	TiXmlDocument defFile;

	bool succeed = defFile.LoadFile( fileName.c_str() );

	if ( succeed )
	{
		ReadClasses( &defFile, collector );
	}
	return succeed;
}

bool XMLParser::ParseDataFile( const std::string& fileName, DataCollector* collector )
{
	TiXmlDocument defFile;

	bool succeed = defFile.LoadFile( fileName.c_str() );

	if ( succeed )
	{
		ReadData( &defFile, collector );
	}
	return succeed;
}

bool XMLParser::SetupIgnoreList( const std::string& fileName, DataCollector* collector )
{
    assert( false ); //"unimplemented functionality"
	return false;
}

int XMLParser::ReadClasses( TiXmlDocument* file, DataCollector* collector )
{
	//error accumulator 
	int errs = 0;

	TiXmlNode* rootNode = NULL;
	TiXmlElement* rootElement = NULL;
	TiXmlNode* classNode = NULL;
	TiXmlElement* classElement = NULL;

	rootNode = file->FirstChild( "Definition" );
	assert( rootNode );
	rootElement = rootNode->ToElement();
	assert( rootElement );


	classNode = rootElement->FirstChild( "class" );
	while ( classNode != NULL )
	{
		classElement = classNode->ToElement();

		if ( classElement )
		{
			errs += ReadClass( classElement, collector );
		}

		classNode = rootNode->IterateChildren( "class", classNode );

	}

	return errs;
}

int XMLParser::ReadClass( TiXmlElement* classElement, DataCollector* collector )
{
	//error accumulator 
	int errs = 0;
	Class* newClass = collector->AddNewClass();
	TiXmlAttribute* attribute = classElement->FirstAttribute();

	while ( attribute )
	{
		if ( !strcasecmp( attribute->Name(), "name" ) )
		{
			newClass->SetName( attribute->Value() );
		}
		else if ( !strcasecmp( attribute->Name(), "Parent" ) )
		{
			newClass->SetParentName( attribute->Value() );
		}
		//else if (!strcasecmp(attribute->Name(), "IDMode"))
		//{
		//	newClass->SetIDMode(!strcasecmp(attribute->Value(), "AutoID") ? Class::ID_AUTO : Class::ID_USER);
		//}
		else
		{
			std::cerr << "DataBase ERROR > Unknown attribute \"" << attribute->Name() << "\"!\n";
			errs++;
		}
		attribute = attribute->Next();
	}

	TiXmlElement* propertyElement = NULL;
	TiXmlNode* propertyNode = NULL;
	propertyNode = classElement->FirstChild( "property" );

	while ( propertyNode != NULL )
	{
		propertyElement = propertyNode->ToElement();
		if ( propertyElement )
		{
			errs += ReadProperty( propertyElement, newClass );
		}
		propertyNode = classElement->IterateChildren( "property", propertyNode );

	}

	newClass->CreatePadding();

	return errs;
}

int XMLParser::ReadProperty( TiXmlElement* propertyElement, Class* pClass )
{
	//error accumulator 
	int errs = 0;
	Property newElement;
	TiXmlAttribute* attribute = propertyElement->FirstAttribute();

	while ( attribute )
	{
		if ( !strcasecmp( attribute->Name(), "name" ) )
		{
			newElement.SetName( attribute->Value() );
		}
		else if ( !strcasecmp( attribute->Name(), "type" ) )
		{
			newElement.SetType( attribute->Value() );
		}
		else if ( !strcasecmp( attribute->Name(), "collection" ) )
		{
			newElement.SetCollection( !strcasecmp( attribute->Value(), "true" ) );
		}
		else if ( !strcasecmp( attribute->Name(), "default" ) )
		{
			newElement.SetDefaultValue( attribute->Value() );
		}
		else
		{
			std::cerr << "DataBase ERROR > Unknown attribute \"" << attribute->Name() << "\"!\n";
			errs++;
		}
		attribute = attribute->Next();
	}

	pClass->AddProperty( newElement );

	return errs;
}

int XMLParser::ReadData( TiXmlDocument* file, DataCollector* collector )
{
	//error accumulator 
	int errs = 0;
	TiXmlNode* rootNode = NULL;
	TiXmlElement* rootElement = NULL;
	rootNode = file->FirstChild( "Data" );
	assert( rootNode );
	rootElement = rootNode->ToElement();
	assert( rootElement );

	TiXmlElement* classElement = NULL;
	TiXmlNode* classNode = NULL;
	classNode = rootElement->FirstChild();
	while ( classNode != NULL )
	{
		classElement = classNode->ToElement();

		if ( classElement )
		{
			const char* className = classElement->Value();
			Class* readClass;
			errs += collector->GetClass( className, readClass );

			if ( readClass )
			{
				Data* newData;
				errs += ReadData( classElement, readClass, newData, collector );
			}
			else
			{
				std::cerr << "DataBase ERROR > Can't find class named \"" << className << "\"!\n";
				errs++;
			}
		}

		classNode = rootElement->IterateChildren( classNode );
	}

	return errs;
}

int XMLParser::ReadData( TiXmlElement* rootClassElement, Class* currentClass, Data*& newData, DataCollector* collector )
{
	//error accumulator 
	int errs = 0;
	TiXmlAttribute* attribute = rootClassElement->FirstAttribute();
	std::string strID;
	while ( attribute )
	{
		if ( !strcasecmp( attribute->Name(), "id" ) )
		{
			strID = attribute->Value();
		}
		else if ( !strcasecmp( attribute->Name(), "class" ) )
		{
			Class* substitute;
			errs += collector->GetClass( attribute->Value(), substitute );
			if ( substitute )
			{
				if ( substitute->IsParent( currentClass, true ) )
				{
					currentClass = substitute;
				}
				else
				{
					std::cerr << "DataBase ERROR > Class \"" << attribute->Value() << "\" is not affiliated to class \"" << currentClass->GetName() << "\"!\n";
					errs++;
				}
			}
			else
			{
				std::cerr << "DataBase ERROR > Can't find class named \"" << attribute->Value() << "\" substitute of \"" << currentClass->GetName() << "\"!\n";
				errs++;
			}
		}
		else
		{
			std::cerr << "DataBase ERROR > Unknown attribute \"" << attribute->Name() << "\"!\n";
			errs++;
		}
		attribute = attribute->Next();
	}

	newData = collector->AddNewData( currentClass );

	if ( !strID.empty() )
	{
		errs += newData->SetID( strID.c_str() );
	}

	// Parse property
	TiXmlElement* propertyElement = NULL;
	TiXmlNode* propertyNode = NULL;
	propertyNode = rootClassElement->FirstChild();

	while ( propertyNode != NULL )
	{
		propertyElement = propertyNode->ToElement();
		if ( propertyElement )
		{
			const char* propName = propertyElement->Value();
			errs += ReadData( propertyElement, currentClass, propName, newData, collector );
		}

		propertyNode = rootClassElement->IterateChildren( propertyNode );
	}

	return errs;
}

int XMLParser::ReadData( TiXmlElement* propertyElement, const Class* currentClass, const char* propName, Data* newData, DataCollector* collector )
{
	//error accumulator 
	int errs = 0;
	const Property* currentProp = currentClass->GetProperty( propName );

	if ( currentProp != NULL )
	{
		if ( propertyElement->GetText() != NULL )
		{
			newData->PushSubData( currentProp, propertyElement->GetText() );
		}
		else if ( currentProp->GetType() == Property::PT_CLASS )
		{
			Class* propertyClass;
			errs += collector->GetClass( currentProp->GetTypeString(), propertyClass );
			if ( propertyClass )
			{
				Data* subData;
				errs += ReadData( propertyElement, propertyClass, subData, collector );
				ClassID* newClassID;
				errs += subData->GetIDByClass( currentProp->GetTypeString(), newClassID );
				if ( newClassID == nullptr )
				{
					// No ID for this instance? make one
					subData->ForceDefaultID();
					subData->GetIDByClass( currentProp->GetTypeString(), newClassID );
					assert( newClassID != nullptr );
				}
				newData->PushSubData( newClassID, currentProp );
			}
		}
	}
	else if ( currentClass->GetParent() )
	{
		ReadData( propertyElement, currentClass->GetParent(), propName, newData->GetParentData(), collector );
	}
	else
	{
		std::cerr << "DataBase WARNING > Unknown property \"" << propName << "\" in class \"" << currentClass->GetName() << "\". Property skipped.\n";
	}

	return errs;
}
