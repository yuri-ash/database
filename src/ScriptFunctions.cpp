#include "DataBase/ScriptFunctions.h"

#if defined(DATABASE_BUILD_STANDALONE)

extern DataCollector g_data;

int GetClassCount()
{
	return g_data.GetClassCount();
}

DataCollector* GetCollector()
{
	return &g_data;
}

#else // DATABASE_BUILD_STANDALONE

int GetClassCount()
{
    return 0;
}

DataCollector* GetCollector()
{
    return nullptr;
}

#endif // DATABASE_BUILD_STANDALONE
