-- A project defines one build target
project "DataBase"
	location ( "build/" .. action )
	kind "ConsoleApp"
	language "C++"
	includedirs {
		"include",
		"depends/libargshandler/include",
		"depends/libexcel/include",
		"depends/libtinyxml/include",
		"depends/liblua/include/lua",
	}
	files {
		"include/DataBase/**.h","src/**.cpp","generated/*.cpp"
	}
	links {
		"ArgumentsHandler",
		"ExcelFormat",
		"Lua",
		"TinyXml",
	}

	configuration { "Release*", "vs2010 or vs2013"}
		postbuildcommands { "copy $(OutDir)\$(ProjectName).exe $(OutDir)..\\..\\.." }
	configuration { "vs2010 or vs2013"}
		buildoptions { "/wd4996 /Dsnprintf=sprintf_s" }
		flags { "NoMinimalRebuild", "FatalWarnings"}
		includedirs {"include/Extra"}
	configuration { "Release*", "gmake" }
		postbuildcommands { "cp ../../bin/gmake/Release/DataBase ../../" }
		-- workaround bug in gcc 4.8 on ubuntu
		-- includedirs { "/usr/include/x86_64-linux-gnu/c++/4.8/" }
	configuration { "Release*", "codeblocks" }
		postbuildcommands { "cp ../../bin/codeblocks/Release/DataBase ../../" }
		-- workaround bug in gcc 4.8 on ubuntu
		-- includedirs { "/usr/include/x86_64-linux-gnu/c++/4.8/" }
	configuration{"gmake or codeblocks"}
		buildoptions { "-m32" }
		buildoptions { "-std=c++11"}
	
