#ifndef __PROPERTYTYPE_H__
#define __PROPERTYTYPE_H__

#include <string>

class CustomType
{
public:
	enum class Archetype
	{
		UNSUPPORTED,
		INTEGER,
		FLOAT,
		CHARACTER,
		BOOLEAN
	};
public:
	CustomType();
	~CustomType();

	inline void SetName(const std::string& name) { m_name = name; }
	inline const std::string& GetName() const { return m_name; }

	inline void SetAlias(const std::string& alias) { m_alias = alias; }
	inline const std::string& GetAlias() const { return m_alias; }

	inline void SetSize(int size) { m_size = size; }
	inline int GetSize() const { return m_size; }

	inline void SetArray( bool isArray ) { m_isArray = isArray; }
	inline bool IsArray() const { return m_isArray; }

	int SetArchetype(const char* options);
	Archetype GetArchetype() const { return m_archetype; }

	inline bool IsString() const
	{
		return m_archetype == Archetype::CHARACTER && m_isArray;
	}

private:

	std::string		m_name;
	std::string		m_alias;
	int				m_size;
	bool			m_isArray;

	Archetype		m_archetype;
};

#endif //__PROPERTYTYPE_H__
