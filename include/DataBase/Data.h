#ifndef __DATA_H__
#define __DATA_H__

#include "Class.h"
#include <map>
#include <cstring>
#include <iostream>

class Data
{
public:
	struct DataValue
	{
		struct DataChunk
		{
			const ClassID*	m_dataID;
			std::string		m_dataString;

			DataChunk(const ClassID* data):m_dataID(data)
			{
				//memset(m_idString, 0, sizeof(m_idString));
			}
			DataChunk(const char* rawData):m_dataID(NULL)
			{
				m_dataString = rawData;
			}
		};

		std::list<DataChunk>	m_values;

		DataValue(const ClassID* data)
		{
			m_values.push_back( DataChunk(data));
		}
		DataValue(const char* rawData)
		{
			m_values.push_back( DataChunk(rawData));
		}


	};
	//struct StringData
	//{
	//	char*		m_string;
	//	const char*	m_name;
	//	StringData(const char* str, const char* name): m_name(name)
	//	{
	//		if (str)
	//		{
	//			m_string = new char[strlen(str) + 1];
	//			strncpy(m_string, str, strlen(str) + 1);
	//		}
	//		else
	//		{
	//			std::cout<<"DataBase WARNING > Argument string empty!\n";
	//			m_string = NULL;
	//		}
	//	}
	//	~StringData()
	//	{
	//		delete m_string;
	//		m_string = NULL;
	//	}
	//};
public:
	Data(Class* relatedClass);
	~Data();

	//void PushData(const char* data, const Property* prop, bool preResolved);
	void ParseDataID(char* buffer, int& offset, const int ID);
	void ParseDataString(char* buffer, int& offset, const char* data, const Property* prop);
	void ParseDataChunk( char* buffer, int& offset, const std::list<Data::DataValue::DataChunk>& dataChunk, const Property* prop );

	void PushSubData(const ClassID* data, const Property* prop);
	void PushSubData(const Property* prop, const char* classID);
    bool RemoveSubData( const Property* property, const ClassID& idToRemove);
    bool RemoveSubData(const Property* property, int32_t index);
    bool RemoveSubData(const Property* property);

	//void PushStringData(const char* data, const char* name){m_subStringData.push_back(new StringData(data, name));}

	inline const char* GetChunk() const {return m_memoryChunk;}

	inline Class* GetClass() const {return m_relatedClass;}

	std::map<const Property*, DataValue>& GetSubData() {return m_subData;}
	const std::map<const Property*, DataValue>& GetSubData()const {return m_subData;}
	//const std::list<StringData*>& GetStringData()const {return m_subStringData;}

    DataValue* GetPropertyData( const Property* property );
    const DataValue* GetPropertyData( const Property* property ) const;

	int SetID(const char* ID);
	int ForceDefaultID();
	inline const ClassID* GetID() const {return m_ID;}

	void CreateParentData();
	inline Data* GetParentData() const { return m_parentData;}

	int GetDataBuffer(char* &buffer);
	int GetDataSize();
	int CreateDataBuffer();
	void FillDataBuffer(char* buffer, int& offset);

	int GetIDByClass(const std::string& className, ClassID*& newClassID)
	{
		if (className == m_relatedClass->GetName())
		{
			newClassID = const_cast<ClassID*>(GetID());
		}
		else if (m_parentData)
		{
			return m_parentData->GetIDByClass(className,newClassID);
		}
		else
		{
			std::cerr << "DATABASE ERROR < Cannot find ID of class \"" << className << "\" in hierarchy of \"" << m_relatedClass->GetName() << "\"\n";
			return 1;
		}
		return 0;
	}

private:
	int ParseDataInteger( char* buffer, int& offset, const char* data, const Property* prop );
	int ParseDataFloat( char* buffer, int& offset, const char* data, const Property* prop );
	int ParseDataCharacter( char* buffer, int& offset, const char* data, const Property* prop );
	int ParseDataBoolean( char* buffer, int& offset, const char* data, const Property* prop );

	int ParseDataCharacter( char* buffer, int& offset, const std::list<Data::DataValue::DataChunk>& dataChunk, const Property* prop );

private:
	//void InitChunkMemory();

	char*			m_memoryChunk;
	//int						m_currentSize;
	Class*					m_relatedClass;
	const ClassID*			m_ID;

	Data*					m_parentData;
	std::map<const Property*, DataValue>	m_subData;
	//std::list<StringData*>	m_subStringData;
};

#endif //__DATA_H__
