#ifndef __XLSWRAPPER_H__
#define __XLSWRAPPER_H__

#include "DataBaseFile.h"
#include "DataBase/Parser.h"
#include "ExcelFormat/BasicExcel.hpp"

#include <vector>
#include <list>

class Element
{
public:
	Element();
	virtual ~Element();

	virtual bool Evaluate(YExcel::BasicExcelWorksheet* currentSheet, int currentLine, TiXmlElement* wrapElement, unsigned int duplicateID)
	{
		return false;
	}

	inline void PushElement(Element* e) { m_properties.push_back(e); }
	inline std::list<Element*>& GetElements() { return m_properties; }

	inline void SetDuplicateID(unsigned int duplicateID) { m_duplicateID = duplicateID; }
	inline unsigned int GetDuplicateID() const { return m_duplicateID; }

protected:
	std::list<Element*>		m_properties;
	unsigned int			m_duplicateID;

};

class CellProperty : public Element
{
public:
	CellProperty();
	virtual ~CellProperty();

	virtual bool Evaluate(YExcel::BasicExcelWorksheet* currentSheet, int currentLine, TiXmlElement* wrapElement, unsigned int duplicateID);

	inline void SetProp(const std::string& prop) { m_prop = prop; }
	inline const std::string& GetProp() const { return m_prop; }

	inline void SetColumnID(int columnID) { m_columnID = columnID; }
	inline int GetColumnID() const { return m_columnID; }
	
	inline void SetSubstituteClass(const std::string& subClass){ m_substituteClass = subClass; }

protected:
	std::string				m_prop;
	std::string				m_substituteClass;
	int						m_columnID;
};

class Switch : public Element
{
public:
	Switch();
	virtual ~Switch();

	virtual bool Evaluate(YExcel::BasicExcelWorksheet* currentSheet, int currentLine, TiXmlElement* wrapElement, unsigned int duplicateID);

	inline void SetColumnID(int columnID){ m_columnID = columnID; }

private:
	int		m_columnID;
};

class Case : public Element
{
public:
	Case();
	virtual ~Case();

	virtual bool Evaluate(YExcel::BasicExcelWorksheet* currentSheet, int currentLine, TiXmlElement* wrapElement, unsigned int duplicateID);

	inline void SetValue(const std::string& value){ m_value = value; }
	inline const std::string& GetValue() const { return m_value; }

private:
	std::string		m_value;
};

class XLSWrapper
{
public:

	//struct ConditionalBlock : public CellProperty
	//{
	//};
	//struct Switch : public ConditionalBlock
	//{
	//	std::map<std::string, CellProperty>	m_properties;
	//};
	struct CellNode
	{
		~CellNode()
		{
			auto currentProperty = m_properties.begin();
			for (; currentProperty != m_properties.end(); ++currentProperty)
			{
				delete *currentProperty;
			}
			m_properties.clear();
		}

		DataBaseFile*			m_file;
		CellProperty			m_root;
		int						m_startLine;
		std::list<Element*>	m_properties;
	};
public:
	XLSWrapper() = delete;
	XLSWrapper(Parser* wrapperParser);
	~XLSWrapper();

	bool OpenFile(const char* fileName);
	void LoadDBFiles(const char* startPathName);
	void Wrap(const char* startPathName);

private:
	void AddDBFile(TiXmlElement* fileElement, const char* startPathName);

	void WrapExcelFile(TiXmlElement* fileElement, const char* startPathName);
	void WrapPage(TiXmlElement* pageElement);
	void WrapNode(CellNode* currentNode, YExcel::BasicExcelWorksheet* currentSheet);
	//bool WrapAttribute(CellProperty* currentProperty, YExcel::BasicExcelWorksheet* currentSheet, int currentLine, TiXmlElement* wrapElement);

	void ReadWrapStructure(CellNode& currentNode, TiXmlElement* nodeElement, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader);
	void AttributeStructureDispatcher(std::list<Element*>& properties, TiXmlElement* nodeElement, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader, std::vector<int>& index);
	void ReadAttributeStructure(std::list<Element*>& properties, TiXmlElement* nodeElement, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader, std::vector<int>& index);
	void ReadSwitchStructure(std::list<Element*>& properties, TiXmlElement* nodeElement, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader, std::vector<int>& index);
	void ReadCaseStructure(std::list<Element*>& properties, TiXmlElement* nodeElement, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader, std::vector<int>& index);

	DataBaseFile* GetFile(const char* id);

	int GetColumnByID(const char* columnID, YExcel::BasicExcelWorksheet* currentSheet, int columnHeader, int startColumn = 0, bool collection = false, int duplicate = 0);
	bool EvaluateColumnID(std::string& strCol, const std::vector<int>& index, const TiXmlAttribute* nodeAttribute);

	//Parser use to read wrapper file and output data
	Parser* m_wrapperParser;

	//CurrentWrapperFile
	TiXmlDocument				m_wrapperFile;

	//XLS file
	YExcel::BasicExcel			m_xlsFile;

	// All XML
	std::list<DataBaseFile*>	m_dbFiles;
};

#endif //__XLSWRAPPER_H__
