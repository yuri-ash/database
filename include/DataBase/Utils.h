#ifndef __UTILS_H__
#define __UTILS_H__

#include <string>
#include "ExcelFormat/BasicExcel.hpp"

std::string MakePath(const std::string& part1, const std::string& part2);
bool IsNumber(const char* str, bool allowFloat);
int CutLine(char* line, int wordsPositions[], int maxWords, char separators[] = "\n\t \"");
char* stoupper( char* s );
std::string evaluateCell(YExcel::BasicExcelWorksheet* currentSheet, int line, int column);

#endif //__UTILS_H__
