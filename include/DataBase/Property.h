#ifndef __PROPERTY_H__
#define __PROPERTY_H__

#include "CustomType.h"
#include <list>

class Property
{
public:
	enum Type
	{
		PT_UNDEF	= -1,
		PT_CUSTOM	= 0,
		PT_CLASS
	};

public:
	Property();
	~Property();

	static inline void AddCustomType(const CustomType& csType) { m_customTypes.push_back(csType); };
	static inline const std::list<CustomType>& GetCustomTypes() { return m_customTypes; }

	inline void SetName(const std::string& name) {m_name = name;}
	inline const std::string& GetName() const {return m_name;}

	void SetType(const char* typeName);
	inline void SetType(Type type) {m_type = type;}
	inline Type GetType() const {return m_type;}
	inline const std::string& GetTypeString() const {return m_typeString;}

	inline void SetCollection(bool collection){m_collection = collection;}
	inline bool IsCollection() const {return m_collection;}

	inline void SetDefaultValue(const std::string& defaultValue){m_defaultValue = defaultValue;}
	inline const std::string& GetDefaultValue() const { return m_defaultValue; }
	inline bool HasDefaultValue() const { return !m_defaultValue.empty() && m_defaultValue.compare("") != 0;}

	unsigned int GetSize() const;
	unsigned int GetBasicSize(int count) const;
	unsigned int GetCustomSize() const;

	const CustomType* GetCustomType() const;

private:
	static std::list<CustomType>	m_customTypes;


	std::string						m_name;
	Type							m_type;
	std::string						m_typeString;
	bool							m_collection;
	std::string						m_defaultValue;
};

#endif //__PROPERTY_H__
