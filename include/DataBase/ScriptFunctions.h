#ifndef __SCRIPTFUNCTIONS_H__
#define __SCRIPTFUNCTIONS_H__

#include "DataCollector.h"

int GetClassCount();
DataCollector* GetCollector();

#endif //__SCRIPTFUNCTIONS_H__