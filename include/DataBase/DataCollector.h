#ifndef __DATACOLLECTOR_H__
#define __DATACOLLECTOR_H__

#include "Class.h"
#include "Data.h"

#include "tinyxml/tinyxml.h"

#ifdef MAX_PATH_LEN
#undef MAX_PATH_LEN
#endif //MAX_PATH_LEN

#define MAX_PATH_LEN 2048

#include <string>
#include <map>

#if defined _WINDOWS
#define strcasecmp(str1, str2)	_stricmp(str1, str2)
#endif //_WINDOWS


struct string_less
{
  bool operator() (const std::string& _Left, const std::string& _Right) const
  {
	  return strcasecmp(_Left.c_str(), _Right.c_str()) < 0 ? true : false;
  }
};

struct ExternId
{
	char m_stringID[512];
	int m_intID;

	ExternId(): m_intID(-1)
	{
		memset(m_stringID, 0, sizeof(m_stringID));
	}
};

class DataCollector
{
public:
	DataCollector();
	~DataCollector();

	// Operation for parser
	Class* AddNewClass();
	Data* AddNewData( Class* instance );

	bool OpenFile(const char* fileName);
	int ReadCustomType();
	int WriteData(const char* destination);
	int QueryExternIDs(const char* filePath);
	int ResolveDependanceIDs();
	int ResolveParenting();
	void SetEncryptKey(const char* key);
	const char* GetEncryptKey();

	inline std::list<Class*>::const_iterator GetFirstClass() const { return m_classes.begin(); }
	inline std::list<Class*>::const_iterator GetLastClass() const { return m_classes.end(); }

	int GetClass( const std::string& className, Class*& newClass );

	inline const Class* FirstClass()
	{
		m_currentClass = m_classes.begin();
		return m_currentClass != m_classes.end() ? *m_currentClass : NULL;
	}
	inline const Class* NextClass()
	{
		++m_currentClass;
		return m_currentClass != m_classes.end() ? *m_currentClass : NULL;
	}

    void GetDerivedClasses( std::list<Class*>& derivedClasses, Class* parentClass);

    inline int GetClassCount(){return static_cast<unsigned int>(m_classes.size());}
	unsigned int GetClassDataCount(const std::string& className);
	
	int ResolveOperation(const char* formula);

	/**
	* Ignore property function
	*/
	void AddToPropertyIgnoreList( const std::string& propertyName );
	bool IsPropertyIgnored( const std::string& propertyName );

private:
	void Reset();
	
	int ReadCustomType(TiXmlElement* typeElement);

	int WriteDataHeader(char* buffer, int offset, const Class* classWrite);

	int QueryExternIDsInFile(const char* fileName);
	int AddExternIDs(const char* stringID, const int intID);
	int SearchForIDs(std::ifstream* currentFile);
	int SearchForIDsCS(std::ifstream* currentFile);
	int ParseEnum(std::ifstream* currentFile);
	int ResolveExternIDs(const char* externID);
	//int ResolveInternIDs(const char* internID, const char* className);
	int ResolveDependanceIDs(Data* data);

	int GetData(Class* classTarget, const char* ID, Data*& newData);

	int ResolveValue(const char* operand);
	int Operate(int operand1, char op, int operand2);

	TiXmlDocument		m_file;

	std::list<Class*>	m_classes;
	std::list<Class*>::iterator m_currentClass;

	std::list<Data*>	m_datas;

	std::map<std::string, int, string_less>		m_externIDs;
	std::list<char*>	m_filesDone;

	std::list<std::string> m_propertyIgnoredList;

	char				m_currentFolder[MAX_PATH_LEN];

	char				m_encryptKey[17];
};

#endif //__DATACOLLECTOR_H__
