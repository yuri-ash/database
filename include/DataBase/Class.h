#ifndef __CLASS_H__
#define __CLASS_H__

#include "Property.h"

struct ClassID
{
	std::string			m_stringID;
	unsigned int		m_intID;

	ClassID():m_intID(0xFFFFFFFF)
	{
	}
};

class Class
{
public:
	enum IDMode
	{
		ID_HASH		= 0,
		ID_NUM
	};

public:
	Class();
	~Class();

	inline void SetName(const std::string& name) { m_name = name; }
	inline const std::string& GetName() const { return m_name; }
	std::string GetObfuscateName() const;

	inline void SetParentName(const std::string& parentName){m_parentName = parentName;}
	inline const std::string& GetParentName() const {return m_parentName;}
	inline void SetParent(Class* parent){m_parent = parent; parent->AddChild(this);}
	inline Class* GetParent() const {return m_parent;}
	inline bool IsParent(const Class* parent, bool distant) const { return m_parent == parent || (distant && m_parent ? m_parent->IsParent(parent, distant) : false); }
	inline void AddChild(Class* child){m_children.push_back(child);}
    inline int GetChildrenCount()const{return static_cast<int>(m_children.size());}

	void AddProperty(const Property& prop);
	inline unsigned int GetClassSize() const {return m_currentSize;}
	//unsigned int GetClassBasicElementSize() const;
	const Property* GetProperty(const std::string& name) const;
	const Property* GetPropertyByType(const std::string& name);
	//unsigned int GetPropertyOffset(const std::string& name) const;
	//unsigned int GetPropertySize(const std::string& name);

	void CreatePadding();

	inline const std::list<Property>& GetProperties() const {return m_properties;}
	
	inline const Property* FirstProperty()
	{
		m_currentProperty = m_properties.begin();
		return m_currentProperty != m_properties.end() ? &(*m_currentProperty) : NULL;
	}
	inline const Property* NextProperty()
	{
		++m_currentProperty;
		return m_currentProperty != m_properties.end() ? &(*m_currentProperty) : NULL;
	}

	inline void SetIDMode(IDMode id) {m_IDMode = id;}
	inline bool IsHashID() const { return m_IDMode == ID_HASH; }

	int AddID(const std::string& ID, ClassID*& newClassID);
	int ValidateIDs();

	inline const std::list<ClassID>& GetIDs() const {return m_IDs;}
	inline const ClassID* FirstID()
	{
		m_currentID = m_IDs.begin();
		return m_currentID != m_IDs.end() ? &(*m_currentID) : NULL;
	}
	inline const ClassID* NextID()
	{
		++m_currentID;
		return m_currentID != m_IDs.end() ? &(*m_currentID) : NULL;
	}
	const ClassID* GetID(const std::string& stringID) const;
	const ClassID* GetID(int intID) const;

private:
	std::string						m_name;
	unsigned int					m_currentSize;
	std::list<Property>				m_properties;
	std::list<Property>::iterator	m_currentProperty;
	IDMode							m_IDMode;
	std::string						m_parentName;
	Class*							m_parent;
	std::list<Class*>				m_children;
	std::list<ClassID>				m_IDs;
	std::list<ClassID>::iterator	m_currentID;

	std::hash<std::string>			m_hasher;
};

#endif //__CLASS_H__
