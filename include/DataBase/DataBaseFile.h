#ifndef __DATABASEFILE_H__
#define __DATABASEFILE_H__

#include <string>
#include "tinyxml/tinyxml.h"

class DataBaseFile
{
public:
	DataBaseFile(const char* filePath, const char* fileName);
	~DataBaseFile();

	inline const std::string& GetID(){return m_id;}

	TiXmlElement* GetProperty(const char* prop, const char* propID);
	TiXmlElement* GetProperty(const char* prop, int count);

private:
	TiXmlElement* GetProperty(TiXmlNode* currentNode, const char* prop, const char* propID);
	TiXmlElement* GetProperty(TiXmlNode* currentNode, const char* prop, int count, int& currentCount);

	TiXmlDocument	m_file;

	std::string		m_id;

};

#endif //__DATABASEFILE_H__
