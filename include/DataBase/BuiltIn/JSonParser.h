#ifndef __JSONPARSER_H__
#define __JSONPARSER_H__

#include "DataBase/Parser.h"
#include "nlohmann/json.hpp"

using json = nlohmann::json;

class Class;
class Data;

/**
* JSONParser class.
* Read and build data base based on xml file.
*/
class JSONParser : public Parser
{
private:
	struct Attributes
	{
		/**
		* Id of this data piece
		*/
		std::string m_ID;

		/**
		* True class of this data, in case of comlex parenting.
		* (we can create an array of some mother class and create derivate object through this data)
		*/
		Class* m_overrideClass;
	};

public:
	/**
	* Default constructor
	*/
	JSONParser();

	/**
	* Destructor
	*/
	~JSONParser() override;

	/**
	* Return true if file extension can be parse using this parser.
	*/
	bool CanReadExtension( const std::string& extension ) const override;

	/**
	* Parse file and add classes to our data collector
	*/
	bool ParseDefinitionFile( const std::string& fileName, DataCollector* collector ) override;

	/**
	* Parse file and add data to our data collector.
	*/
	bool ParseDataFile( const std::string& fileName, DataCollector* collector ) override;
	
	/**
	* Setup an ignore list to avoid emitting error when finding property in data that are not part of the class.
	*/
	bool SetupIgnoreList( const std::string& fileName, DataCollector* collector ) override;

private:
	/**
	* Parse all classes definition in json struct
	*/
	bool ParseClassesDefinition( const json& jsonDef, DataCollector* collector );

	/**
	* Parse one clas def and add it to collector.
	*/
	bool ParseClassDefinition( const json& jsonDef, DataCollector* collector );

	/**
	* Parse class attributes. How this class behave in the system.
	*/
	bool ParseClassAttributes( const json& jsonDef, Class* newClass );

	/**
	* Parse class properties. What this class is made of.
	*/
	bool ParseClassProperties( const json& jsonDef, Class* newClass, DataCollector* collector );

	/**
	* Parse all data definition from this json struct.
	*/
	bool ParseDataDefinition( const json& jsonDef, DataCollector* collector );

	/**
	* Parse a specific Data Class.
	*/
	bool ParseDataClass( const json& jsonDef, Class* dataClass, Data* &newData, DataCollector* collector );

	/**
	* Parse all attributes of a given data. (ID and such)
	*/
	bool ParseDataAttributes( Attributes &dataAttibutes, const json& jsonDef, Class* &currentClass, DataCollector* collector );

	/**
	* Parse one property of a data.
	*/
	bool ParseDataProperty( const json& jsonDef, const std::string& propertyName, Class* currentClass, Data* currentData, DataCollector* collector );
};

#endif //__JSONPARSER_H__