#ifndef __XMLPARSER_H__
#define __XMLPARSER_H__

#include "DataBase/Parser.h"

class Class;
class Data;
class TiXmlDocument;
class TiXmlElement;

/**
* XMLParser class. 
* Read and build data base based on xml file.
*/
class XMLParser : public Parser
{
public:
	/**
	* Default constructor
	*/
	XMLParser();

	/**
	* Destructor
	*/
	~XMLParser() override;

	/**
	* Return true if file extension can be parse using this parser.
	*/
	bool CanReadExtension( const std::string& extension ) const override;

	/**
	* Parse file and add classes to our data collector
	*/
	bool ParseDefinitionFile( const std::string& fileName, DataCollector* collector ) override;

	/**
	* Parse file and add data to our data collector.
	*/
	bool ParseDataFile( const std::string& fileName, DataCollector* collector ) override;

	/**
	* Setup an ignore list to avoid emitting error when finding property in data that are not part of the class.
	*/
	bool SetupIgnoreList( const std::string& fileName, DataCollector* collector ) override;

private:
	/**
	* Parse all classes in the file
	*/
	int ReadClasses( TiXmlDocument* file, DataCollector* collector );

	/**
	* Read one class properties
	*/
	int ReadClass( TiXmlElement* classElement, DataCollector* collector );

	/**
	* Read one class property
	*/
	int ReadProperty( TiXmlElement* propertyElement, Class* pClass );

	/**
	* Read a data file
	*/
	int ReadData( TiXmlDocument* file, DataCollector* collector );

	/**
	* Read data
	*/
	int ReadData( TiXmlElement* rootClassElement, Class* currentClass, Data*& newData, DataCollector* collector );

	/**
	* Read data
	*/
	int ReadData( TiXmlElement* propertyElement, const Class* currentClass, const char* propName, Data* newData, DataCollector* collector );
};

#endif //__XMLPARSER_H__