#ifndef DATABASE_DATAVERIFICATION_H
#define DATABASE_DATAVERIFICATION_H

#include <iostream>
#include <string>

extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
extern int luaopen_DataBase( lua_State* L );
}

class DataVerification
{
	DataVerification();
	static DataVerification* m_dataVerifInstance;

public:
	~DataVerification();

	static DataVerification* GetDataVerification();

	bool Init( const std::string& file );
	bool Deinit( );

	template<typename T>
	bool IsValid( const std::string& typeName, T* value )
	{
		std::string funcName = "validate_" + typeName;
		lua_getglobal( m_validateState, funcName.c_str() );
		if ( lua_isfunction( m_validateState, -1 ) )
		{
			lua_pushlightuserdata( m_validateState, value );
			if ( lua_pcall( m_validateState, 1, 0, 0 ) != 0 )
			{
				std::cerr << "DataBaseError > validation " << lua_tostring( m_validateState, -1 ) << std::endl;
			}
		}
		else
		{
			// No function to validate data, hence data is valid
			lua_pop( m_validateState, 1 );
		}
		return true;
	}

	bool IsValid( const std::string& typeName, char* value );

private:
	lua_State* m_validateState;
};

#endif //DATABASE_DATAVERIFICATION_H