#ifndef DATABASE_ISSUEREPORT_H
#define DATABASE_ISSUEREPORT_H

#include <stdint.h>

/**
* Log issue and count error and warning to remember what happen during a run.
*/
class IssueReporter
{
public:
	enum class Severity
	{
		S_CRITICAL,
		S_ERROR,
		S_WARNING,
		S_LOG
	};

public:
	/**
	* Constructor
	*/
	IssueReporter();

	/**
	* Destructor
	*/
	~IssueReporter();

	/**
	* Log an error and bump it's count.
	* If Status is "Critical" then it will bump critical issues count too.
	*/
	void ReportError( Severity status, const wchar_t* errorLog, ... );
	void ReportError( Severity status, const char* errorLog, ... );

	/**
	* Log a warning and bump it's count.
	* If Status is "Critical" then it will bump critical issues count too.
	*/
	void ReportWarning( Severity status, const wchar_t* warningLog, ... );
	void ReportWarning( Severity status, const char* warningLog, ... );

	/**
	* Get critical issues count
	*/
	inline uint32_t GetCriticalIssuesCount() const
	{
		return m_criticalIssuesCount;
	}

	/**
	* Get errors count
	*/
	inline uint32_t GetErrorsCount() const
	{
		return m_errorsCount;
	}

	/**
	* Get warnings count
	*/
	inline uint32_t GetWarningsCount() const
	{
		return m_warningsCount;
	}

	/**
	* Print out collected information.
	*/
	void PrintReport() const;

/** Static function **/
public:
	static IssueReporter& GetReporter();

private:
	/**
	* Add +1 to current error count
	*/
	inline void BumpError()
	{
		m_errorsCount++;
	}

	/**
	* Add +1 to current warning count
	*/
	inline void BumpWarning()
	{
		m_warningsCount++;
	}

private:
	/**
	* Critical issue can be either error or warning based on severity passed to Report* functions.
	* This value should be 0 for tool to consider it's job done without issue, even if error have been reported.
	*/
	uint32_t m_criticalIssuesCount;

	/**
	* Count all errors reported
	*/
	uint32_t m_errorsCount;

	/**
	* Count all warning reported
	*/
	uint32_t m_warningsCount;

	/**
	* Determine which level should be consider critical.
	* Any value under this one will be consider a critical issue.
	*/
	Severity m_criticalLevel;
};

#define LogWarning( msg, ...) IssueReporter::GetReporter().ReportWarning(IssueReporter::Severity::S_WARNING, msg, __VA_ARGS__);
#define LogError( msg, ...) IssueReporter::GetReporter().ReportError(IssueReporter::Severity::S_ERROR, msg, __VA_ARGS__);
#define LogCriticalError( msg, ...) IssueReporter::GetReporter().ReportError(IssueReporter::Severity::S_CRITICAL, msg, __VA_ARGS__);
#endif //DATABASE_ISSUEREPORT_H