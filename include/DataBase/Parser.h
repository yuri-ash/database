#ifndef __PARSER_H__
#define __PARSER_H__

#include <string>

class DataCollector;

/**
* Parser class. Help parsing data to feed to database in order to transform into binary.
* Two default parser are build-in, xml and json. An extension of the code is planned to be able to load
* other parser to fit any user purpose.
*/
class Parser
{
public:
	/**
	* Default constructor
	*/
	Parser();

	/**
	* Destructor
	*/
	virtual ~Parser();

	/**
	* Return true if file extension can be parse using this parser.
	*/
	virtual bool CanReadExtension( const std::string& extension ) const = 0;

	/**
	* Parse file and add classes to our data collector
	*/
	virtual bool ParseDefinitionFile( const std::string& fileName, DataCollector* collector ) = 0;

	/**
	* Parse file and add data to our data collector.
	*/
	virtual bool ParseDataFile( const std::string& fileName, DataCollector* collector ) = 0;

	/**
	* Setup an ignore list to avoid emitting error when finding property in data that are not part of the class.
	*/
	virtual bool SetupIgnoreList( const std::string& fileName, DataCollector* collector ) = 0;
};

#endif //__PARSER_H__